@isTest
public class AllClinicsInformation_Webservice_Test {
    
    @testSetup static void setup123() {
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        Account objAccount = new Account();
        objAccount.Name = 'clinic1';
        objAccount.BillingPostalCode = '1234567';
        objAccount.Phone = '9329423949';
        objAccount.Email__c = 'testnew@gmail.com';
        insert objAccount;
        
        Contact businessContact = new Contact();
        businessContact.AccountId = objAccount.Id;
        businessContact.LastName = 'Apex';
        businessContact.FirstName = 'Test';
        insert businessContact;
    }
    @isTest
    public static void AllCliInfWebserviceTest(){
        
        String requestJson = '{"clinicName":"clinic1","zipCode":"1234567","doctorName":"Apex","specialty":"heart"}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getAllClinics';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	AllClinicsInformation_Webservice.getAllClinicsInformation();
        test.stopTest();
    }
    @isTest
    public static void AllCliInfWebserviceTest1(){
        String requestJson = '{"clinicName":"","zipCode":"1234567","doctorName":"","specialty":""}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getAllClinics';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	AllClinicsInformation_Webservice.getAllClinicsInformation();
        test.stopTest();
    }
    
    @isTest
    public static void AllCliInfWebserviceTest2(){
        String requestJson = '{"clinicName":"clinic1","zipCode":"","doctorName":"","specialty":""}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getAllClinics';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	AllClinicsInformation_Webservice.getAllClinicsInformation();
        test.stopTest();
    }
}