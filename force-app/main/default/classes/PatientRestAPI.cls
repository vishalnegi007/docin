@RestResource(urlMapping='/PatientAPI/*')
global with sharing class PatientRestAPI{
    // Post Method
    @HTTPPost
    global static Response  createPatient() {
        // Initializing the request parameter with the incoming request
        Response response = new Response();
        try {
            
            RestRequest patientRequest = RestContext.request;
            // Getting the request body from the request
            String requestBody = patientRequest.requestBody.toString();
            // Deserializing the JSON response body and assigning it to an instance of Contact object
            patientRecord record = (patientRecord) JSON.deserialize(requestBody, patientRecord.class);
            // Inserting patient
            System.debug(record+'record12321');
            Patient__c obj = new Patient__c();
            List<Patient__c> userList = new List<Patient__c>();

            if(record.isSocialLogin){
                userList = [SELECT Id,Name,Username__c,Email__c,Social_ID__c FROM Patient__c 
                            WHERE Social_ID__c=:record.socialId
                            OR (Email__c=:record.Email OR Username__c =:record.Username)
                            LIMIT 1];
            }else{
                userList = [SELECT Id,Name,Username__c,Email__c FROM Patient__c 
                            WHERE Email__c=:record.Email OR Username__c =:record.Username
                            LIMIT 1];
            }

            
            if(!userList.isEmpty()){
                response.message = 'User already exist with same email/username';
                response.status_code = '400';
                response.data = userList[0];
                return response;
            }
            
            obj.Name = record.FirstName;
            obj.Last_Name__c = record.LastName;
            obj.Email__c = record.Email;
            obj.Gender__c = record.Gender;
            obj.Phone__c = record.Phone;
            obj.Username__c = record.Username;
            obj.Password__c = record.Password;
            if(String.isNotBlank(record.dateOfBirth))
                obj.Date_of_Birth__c = Date.valueOf(record.dateOfBirth);
            if(record.isSocialLogin){
                obj.Social_ID__c = record.socialId;
            }

            
            insert obj;
            
            response.message = 'User Created Successfully';
            response.status_code = '200';
            response.data = obj;
            
        } catch(Exception exe){
            System.debug(exe.getMessage()+'getMessage');
            response.message = 'Error in creating User';
            response.status_code = '400';
        }
        return response;
    }
    
    global class Response{
        global String message;
        global String status_code;
        global Patient__c data;
    }
    
    public class patientRecord{
        
        public String FirstName;
        public String LastName;
        public String Email;
        public String Gender;
        public String Phone;
        public String Username;
        public String Password;
        public String dateOfBirth;
        public Boolean isSocialLogin;
        public String socialId;
    }
    
}