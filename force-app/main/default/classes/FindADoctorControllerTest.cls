@isTest
public class FindADoctorControllerTest {
    
    @testSetup static void setup123() {
        Patient__c patient = New Patient__c();
        patient.Name = 'Test';
        insert patient;
        
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        Account objAccount = new Account();
        objAccount.Name = 'clinic1';
        objAccount.BillingPostalCode = '1234567';
        objAccount.Phone = '9329423949';
        objAccount.Email__c = 'testnew@gmail.com';
        objAccount.Clinic_Image_1__c = 'test';
        objAccount.Clinic_Image_2__c = 'test';
        objAccount.Clinic_Image_3__c = 'test';
        objAccount.Clinic_Image_4__c = 'test';
        objAccount.Clinic_Image_5__c = 'test';
        insert objAccount;
        
        Contact businessContact = new Contact();
        businessContact.AccountId = objAccount.Id;
        businessContact.LastName = 'Apex';
        businessContact.FirstName = 'Test';
        businessContact.Email = 'Test@gmail.com';
        businessContact.Specialty__c = 'Primary Care';
        insert businessContact;
        
        Service_Provider__c serviceProvider = New Service_Provider__c();
		serviceProvider.Service_Provider__c = objAccount.Id;
        serviceProvider.Contact__c = businessContact.Id;
        insert serviceProvider;
        
        Test_Procedures__c testProcedures = New Test_Procedures__c();
        testProcedures.Service_Provider__c = objAccount.Id;
        insert testProcedures;
    }
    
	@isTest
    public static void getAllDoctorsInfoTest(){
        Test.startTest();
        FindADoctorController.getAllDoctorsInfo();
        Test.stopTest();
    }
    
    @isTest
    public static void getUserInformationTest(){
        List<Patient__c> patientList = New List<Patient__c>([select id from Patient__c limit 1]);
        if(!patientList.isEmpty()) {
            Test.startTest();
            FindADoctorController.getUserInformation(patientList[0].Id);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void getSpecialtiesAndTestsTest(){
        List<Account> AccountList = New List<Account>([select id from Account limit 1]);
        if(!AccountList.isEmpty()) {
            Test.startTest();
            FindADoctorController.getSpecialtiesAndTests(AccountList[0].Id);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void getDoctorInfoTest(){
        List<Contact> ContactList = New List<Contact>([select id from Contact limit 1]);
        if(!ContactList.isEmpty()) {
            Test.startTest();
            FindADoctorController.getDoctorInfo(ContactList[0].Id);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void newFindADoctorTest(){
        Test.startTest();
        FindADoctorController.newFindADoctor('', '', '', '');
        Test.stopTest();
    }
    
    @isTest
    public static void searchClinicsInformationTest(){
        List<Account> AccountList = New List<Account>([select Id, Name, BillingPostalCode from Account limit 1]);
        List<Contact> ContactList = New List<Contact>([select Id, Specialty__c, FirstName from Contact limit 1]);
        if(!AccountList.isEmpty() && !ContactList.isEmpty()) {
            Test.startTest();
            FindADoctorController.searchClinicsInformation(AccountList[0].Name, ContactList[0].Specialty__c, '', AccountList[0].BillingPostalCode);
            FindADoctorController.searchClinicsInformation(AccountList[0].Name, '', ContactList[0].FirstName, '');
            FindADoctorController.searchClinicsInformation('', '','', AccountList[0].BillingPostalCode);
            FindADoctorController.searchClinicsInformation('', ContactList[0].Specialty__c, ContactList[0].FirstName, AccountList[0].BillingPostalCode);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void createAppointmentRecordTest(){
        List<Patient__c> PatientList = New List<Patient__c>([select Id from Patient__c limit 1]);
        List<Contact> ContactList = New List<Contact>([select Id from Contact limit 1]);
        String appointmentJSON = '{"Patient__c":"'+PatientList[0].Id+'", "Doctor__c":"'+ContactList[0].Id+'", "Patient_Name__c":"Test", "Appointment_Date__c":"2021-08-16", "Appointment_Time__c":"16:00:00.000Z"}';
        if(!PatientList.isEmpty() && !ContactList.isEmpty()) {
            Test.startTest();
            FindADoctorController.createAppointmentRecord(appointmentJSON);
            Test.stopTest();
        }
    }
    
    @isTest
    public static void getRatingsTest(){
        Test.startTest();
        FindADoctorController.getRatings('');
        Test.stopTest();
    }
    
    @isTest
    public static void getAllAppointmentsTest(){
        Test.startTest();
        FindADoctorController.getAllAppointments('');
        Test.stopTest();
    }

	@isTest
    public static void updateUserProfileTest(){
        List<Patient__c> PatientList = New List<Patient__c>([select Id from Patient__c limit 1]);
        if(!PatientList.isEmpty()) {
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
            FindADoctorController.updateUserProfile(PatientList[0]);
            //FindADoctorController.uploadUserProfilePhoto('imageData',PatientList[0].Id);
            Test.stopTest();
        }
    }
    
    /*@isTest
    public static void uploadUserProfilePhotoTest(){
        List<Patient__c> patientList = New List<Patient__c>([select id from Patient__c limit 1]);
        if(!patientList.isEmpty()) {
            Test.startTest();
            FindADoctorController.uploadUserProfilePhoto('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==', patientList[0].Id);
            Test.stopTest();
        }
    }*/
    
    @isTest
    public static void getSearchFiltersTest(){
        Test.startTest();
        FindADoctorController.getSearchFilters();
        Test.stopTest();
    }
    
}