@isTest
public class AddRatingWebserviceTest {
	@isTest
    public static void AddRatingTest(){
        
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        Account objAccount = new Account();
        objAccount.Name = 'clinic1';
        objAccount.BillingPostalCode = '1234567';
        objAccount.Phone = '9329423949';
        objAccount.Email__c = 'testnew@gmail.com';
        insert objAccount;
        
        Contact businessContact = new Contact();
        businessContact.AccountId = objAccount.Id;
        businessContact.LastName = 'Apex';
        businessContact.FirstName = 'Test';
        insert businessContact;
        
        Patient__c patient = New Patient__c();
        patient.Name = 'Test';
        insert patient;
        
        String requestJson = '{"doctorId":"'+businessContact.Id+'","userId":"'+patient.Id+'","rating":"1.2","details":"heart"}';
        //String requestJson = '{"doctorId":"4324ewdsadas","userId":"4324ewdsadas","rating":"1.2","details":"heart"}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/addRating';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        AddRatingWebservice.addRating();
        test.stopTest();
        List<Rating__c> lstrating = [select id, name from Rating__c];
        System.assertEquals(lstrating.size(),1);
    }
}