global class HttpMockCallout implements HttpCalloutMock{
    global HttpResponse respond(HTTPRequest req){
        HttpResponse res = new HttpResponse();
        res.setStatus('OK');
        res.setStatusCode(200);
        res.setBody('{"webContentLink":"contentLink","id": "cus_K2m7JfpZ2mvuZf","object": "customer","address": null,"balance": 0,"created": 1629023253,"currency": null,"default_source": null,"delinquent": false,"description": null,"discount": null,"email": "test@gmail.com","invoice_prefix": "5E13DBEC","invoice_settings": {"custom_fields": null,"default_payment_method": null,"footer": null},"livemode": false,"metadata": {"accountId": "0010q00000b9pEnAAI"},"name": "test","next_invoice_sequence": 1,"phone": "312312312312312","preferred_locales": [],"shipping": null,"tax_exempt": "none"}');
        return res;
    }
}