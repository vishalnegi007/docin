@RestResource(urlMapping='/createAppointment/*')
global with sharing class BookAnAppointmentWebservice {
    @HTTPPost
    global static Response createAppointmentRecord() {
        Appointment__c appointment = new Appointment__c();
        Response response = new Response();
        try{
            RestRequest req = RestContext.request;
            Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            appointment.Patient_Name__c = params.get('name').toString();
            if(String.isNotBlank(params.get('patientId').toString()))
                appointment.Patient__c = params.get('patientId').toString();
            appointment.Doctor__c = params.get('doctorId').toString();
            appointment.Patient_Email__c = params.get('email').toString();
            appointment.Patient_Phone__c = params.get('phone').toString();
            appointment.Appointment_Date__c = Date.valueOf(params.get('date').toString());
            appointment.Appointment_Time__c =  Time.newInstance(Integer.valueOf(params.get('time').toString().split(':')[0]),
                                                                Integer.valueOf(params.get('time').toString().split(':')[1]),
                                                                0, 0);
            insert appointment;
            appointment = [SELECT Id,Name,Specialty__c,Doctor__r.Account.Name,Patient_Name__c,Patient_Email__c,Appointment_Date__c,Appointment_Time__c,Patient_Phone__c, Patient__c, Patient__r.Name, Patient__r.Last_Name__c, Doctor__c,Doctor__r.Title, Doctor__r.FirstName,Doctor__r.LastName
                           FROM Appointment__c 
                           WHERE Id = :appointment.Id
                           LIMIT 10000];
            Contact doctor = [SELECT Id,Name,Email,FirstName,LastName,Account.Email__c,Account.Name FROM Contact WHERE Id = :appointment.Doctor__c LIMIT 1];
            //Sending Emails to Patient and Doctor
            FindADoctorController.sendAppointmentEmails(doctor,appointment);
            response.message = 'Booking Completed Successfully';
            response.status_code = '200';
            response.data = appointment;
        }catch(Exception ex){
            response.message = 'Error in Booking Appointment'+ex.getMessage();
            response.status_code = '400';
            response.data = appointment;
        }
        return response;
    }
    global class Response{
        public String message;
        public String status_code;
        public Appointment__c data;
    }
}