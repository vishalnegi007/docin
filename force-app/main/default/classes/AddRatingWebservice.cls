@RestResource(urlMapping='/addRating/*')
global with sharing class AddRatingWebservice{
    // Post Method
    @HTTPPost
    global static Response addRating() {
        Response response = new Response();
        Rating__c rating = new Rating__c();
        try{
            RestRequest patientRequest = RestContext.request;
            // Getting the request body from the request
            String requestBody = patientRequest.requestBody.toString();
            // Deserializing the JSON response body and assigning it to an instance of Contact object
            Map<String,Object> params = (Map<String,Object>) JSON.deserializeUntyped(requestBody);
            
            //Creating a new Rating Record
            rating.Doctor__c = params.get('doctorId').toString();
            rating.User__c = params.get('userId').toString();
            rating.Ratings__c = Decimal.valueOf(params.get('rating').toString());
            rating.Description__c = params.get('details').toString();
            System.debug('Rating::'+rating);
            insert rating;
            response.message = 'Ratings Added Successfully';
            response.status_code = '200';
            response.rating = rating;
        }catch(Exception ex){
            response.message = 'Error in Adding Rating ';
            response.status_code = '404';
            response.rating = rating;
        }
        return response;
    }
    
    global class Response{
        public String message;
        public String status_code;
        public Rating__c rating;
    }
}