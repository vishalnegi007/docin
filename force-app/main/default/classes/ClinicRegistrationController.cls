public without sharing class ClinicRegistrationController {
    
    public static Boolean isValidPassword(UserWrapper userInfo){
        // String password, String confirmPassword
        return userInfo.password==userInfo.confirmPassword;
    }

    @AuraEnabled
    public static RegistrationWrapper registerUser(String userInfo){
        RegistrationWrapper registrationWrapper = new RegistrationWrapper();
        UserWrapper userWrap = (UserWrapper) System.JSON.deserialize(userInfo, UserWrapper.class);
        try {
            if(!isValidPassword(userWrap)){
                registrationWrapper.isValid = false;
                registrationWrapper.message = 'Password does not match';
                return registrationWrapper;
            }
            List<Account> clinicList = new List<Account>();
            clinicList = [SELECT Id,Name FROM Account WHERE Name=:userWrap.practiceName LIMIT 10];
            if(clinicList.size()>0){
                registrationWrapper.isValid = false;
                registrationWrapper.message = 'Practice name already exists';
                return registrationWrapper;
            }
            User adminUser = [SELECT ID,FirstName FROM User WHERE Profile.Name='System Administrator' LIMIT 1];
            Account acc = new Account();
            acc.Name = userWrap.practiceName;
            insert acc;
            //Converting Account into Partner Account
            acc.isPartner = true;
            acc.OwnerId = adminUser.Id;
            update acc;
            
            system.debug('AccountId::'+acc.Id +' :: '+ acc.Name);
            
            List<UserRole> userRole = [SELECT ID,Name FROM UserRole WHERE DeveloperName = 'CEO' limit 1];
            List<Profile> profile = [SELECT ID,Name FROM Profile WHERE Name = 'Partner Community User Copy' limit 1];
            String profileId = profile[0].Id; // To be filled in by customer.
            String roleEnum = userRole[0].Id; // To be filled in by customer.
            String accountId = acc.Id; // To be filled in by customer.
            
            String userName = userWrap.email;
            
            User u = new User();
            u.Username = userWrap.userName;
            u.Email = userWrap.email;
            u.FirstName = userWrap.firstName;
            u.LastName = userWrap.lastName;
            u.CommunityNickname = u.FirstName + u.LastName;
            u.ProfileId = profileId;
            String userId;
            
            try {
                userId = Site.createExternalUser(u, accountId, userWrap.password);
            } catch(Site.ExternalUserCreateException ex) {
                List<String> errors = ex.getDisplayMessages();
                String message = '';
                for(String msg : errors){
                    message += msg +' - ';
                }                
                registrationWrapper.message = message;
                return registrationWrapper;
                // This message is used for debugging. Do not display this in the UI to the end user.
                // It has the information around why the user creation failed.
            }
            System.debug('userId--> '+userId);

            if (userId != null) { 
                if (userWrap.password != null && userWrap.password.length() > 1) {
                    //return Site.login(userWrap.userName, userWrap.password, ApexPages.currentPage().getParameters().get('startURL'));
                }
                else {
                    PageReference page = System.Page.CommunitiesSelfRegConfirm;
                    page.setRedirect(true);
                   // return page;
                }
            }
            
            registrationWrapper.isValid = true;
            registrationWrapper.message = 'User Create Successfully';
            registrationWrapper.userObject = userId;
            return registrationWrapper;
        }catch (Exception e) {
            registrationWrapper.message = e.getLineNumber()+e.getMessage();
            return registrationWrapper;
        }
    }
    @AuraEnabled
    public static List<Subscription_Plan__c> getSubscriptionPlans(){
        try {
            List<Subscription_Plan__c> allSubscriptions = new List<Subscription_Plan__c>();
            allSubscriptions = [SELECT Id,Name,Plan_Charges__c FROM Subscription_Plan__c ORDER BY Plan_Charges__c ASC LIMIT 100];
            return allSubscriptions;
        } catch (Exception e) {
            return null;
        }
    }

    public class RegistrationWrapper{
        @AuraEnabled
        public Boolean isValid;
        @AuraEnabled
        public String message;
        @AuraEnabled
        public String userObject;
    }
    public class UserWrapper{
        public String practiceName;	//
        public String firstName;	//
        public String lastName;	//
        public String userName;	//
        public String email;	//
        public String password;	//
        public String confirmPassword;	//
    }
}