@isTest
public class UpdateProfile_WeserviceTest {
	@isTest
    public static void UpdateProfileTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"userId":"'+objPatien.Id+'","firstName":"Test Firstname1","lastName":"Test Lastname1","email":"test11@gmail.com","phone":"1234567890","gender":"Male","gender":"Male","dateOfBirth":"1990-02-17","userimage":"uyyweuiwqeuqwe6qw8eqwieywqiuyewuqi"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/updateProfile';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	UpdateProfile_Weservice.updateProfile();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
    }
    
    @isTest
    public static void UpdateProfileTest12(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"userId":,"firstName":"Test Firstname1","lastName":"Test Lastname1","email":"test11@gmail.com","phone":"1234567890","gender":"Male","gender":"Male","dateOfBirth":"1990-02-17","userimage":""}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/updateProfile';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	UpdateProfile_Weservice.updateProfile();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
    }
    
    @isTest
    public static void UpdateProfileTest234(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"userId":"","firstName":"Test Firstname1","lastName":"Test Lastname1","email":"test11@gmail.com","phone":"1234567890","gender":"Male","gender":"Male","dateOfBirth":"1990-02-17","userimage":""}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/updateProfile';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	UpdateProfile_Weservice.updateProfile();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
    }
}