@isTest
public class PatientRestAPITest {
	@isTest
    public static void PatientRestTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"isSocialLogin":true,"socialId":"1234567890","Email":"test11@gmail.com","Username":"Test patient17666","FirstName":"Test First1","LastName":"Test Last1","dateOfBirth":"1990-02-17","Phone":"1234567890","Gender__c":"Male"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/PatientAPI';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	PatientRestAPI.createPatient();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
    }
    @isTest
    public static void PatientRestTest12(){
        /*Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient1';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;*/
        
        String requestJson = '{"isSocialLogin":false,"socialId":"1234567890","Email":"test1441@gmail.com","Username":"Test123 patient17666","FirstName":"Test First1","LastName":"Test Last1","dateOfBirth":"1990-02-17","Phone":"1234567890","Gender__c":"Male"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/PatientAPI';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	PatientRestAPI.createPatient();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        
        System.assertEquals(lstPatien.size(),1);
    }
    @isTest
    public static void PatientRestTest23(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"isSocialLogin":"false","socialId":"1234567890","Email":"test1441@gmail.com","Username":"Test123 patient17666","FirstName":"Test First1","LastName":"Test Last1","dateOfBirth":"1990-02-17","Phone":"1234567890","Gender__c":"Male"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/PatientAPI';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	PatientRestAPI.createPatient();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.debug('lstPatien::::'+lstPatien);
        System.assertEquals(lstPatien.size(),1);
    }
}