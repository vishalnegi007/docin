global class CreateStripeAccountsBatch implements Database.AllowsCallouts,Database.Batchable<Account>{
    
    List<Account> accountList = New  List<Account>();
    //Constructor to initialize the values from trigger
    global CreateStripeAccountsBatch (List<Account> accList){
        accountList = accList;
    }
    
    // Start Method
    global List<Account> start(Database.BatchableContext BC){
        return accountList;
    }
    
    // Execute Method
    global void execute(Database.BatchableContext BC, List<Account>scope){
        Map<Id,Account> accountToUpdate = new Map<Id,Account>([SELECT Id,Name,Email__c,Phone FROM Account WHERE ID IN :accountList LIMIT 50000]);
        Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
        Stripe_Credentials__c createCustomerAPI = Stripe_Credentials__c.getValues('Create Customer');
        for(Account accountRecord : accountList){
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(createCustomerAPI.Endpoint__c);
            request.setMethod('POST');    
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
            String payload = '';
            payload += 'name='+ EncodingUtil.urlEncode(accountRecord.Name,'UTF-8'); 
            if(String.isNotBlank(accountRecord.Email__c))
                payload += '&email='+ EncodingUtil.urlEncode(accountRecord.Email__c,'UTF-8');
            if(String.isNotBlank(accountRecord.Phone))
                payload += '&phone='+ EncodingUtil.urlEncode(accountRecord.Phone,'UTF-8');
            payload += '&metadata[accountId]='+ EncodingUtil.urlEncode(accountRecord.Id,'UTF-8');
            // if(String.isNotBlank(accountRecord.BillingStreet))
            //   payload += '&shipping[address][line1]='+ EncodingUtil.urlEncode(accountRecord.BillingStreet,'UTF-8');
            // // payload += '&billing_details[address][line2]='+ EncodingUtil.urlEncode(accountRecord.Email__c,'UTF-8');
            // if(String.isNotBlank(accountRecord.BillingCity))
            //   payload += '&shipping[address][city]='+ EncodingUtil.urlEncode(accountRecord.BillingCity,'UTF-8');
            // if(String.isNotBlank(accountRecord.BillingState))
            //   payload += '&shipping[address][state]='+ EncodingUtil.urlEncode(accountRecord.BillingState,'UTF-8');
            // if(String.isNotBlank(accountRecord.BillingCountry))
            //   payload += '&shipping[address][country]='+ EncodingUtil.urlEncode(accountRecord.BillingCountry,'UTF-8');
            // if(String.isNotBlank(accountRecord.BillingPostaLCode))
            //   payload += '&shipping[address][postal_code]='+ EncodingUtil.urlEncode(accountRecord.BillingPostaLCode,'UTF-8');
            
            System.debug('payload:::'+payload);
            request.setBody(payload);
            HttpResponse response = http.send(request);
            // Parse the JSON response
            if (response.getStatusCode() != 200) {
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            } else {
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                accountToUpdate.get(accountRecord.Id).Stripe_Customer_Account__c = myMap.get('id').toString();
            }
        }
        System.debug('accountToUpdate---> '+accountToUpdate);
        update accountToUpdate.values();
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}