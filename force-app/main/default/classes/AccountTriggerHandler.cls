public class AccountTriggerHandler {
    public static void createStripeAccount(List<Account> accountList) {
        List<Account> accountListToPass = new List<Account>();
        for(Account acc : accountList){
            accountListToPass.add(acc);
        }
        Database.executeBatch(new CreateStripeAccountsBatch(accountListToPass));
    }
}