@RestResource(urlMapping='/authLogin/*')
global with sharing class AuthenticateLoginWebservice {
    @HTTPPost
    global static AuthenticationResponse authenticatePatient(){
        RestRequest req = RestContext.request;
        List<Patient__c> patientsList = new List<Patient__c>();
        AuthenticationResponse response = new AuthenticationResponse();
        Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
        
        String username = params.get('Username').toString();
        String password = params.get('Password').toString();

        patientsList = [SELECT Id,Name,Last_Name__c,Email__c,Gender__c,Password__c,Phone__c,Username__c
                        FROM Patient__c
                        WHERE Username__c =:username
                        AND Password__c =:password
                        LIMIT 1];
        
        if(patientsList.isEmpty()){
            response.message = 'Unable to find User';
            response.data = new Patient__c();
            response.status_code = '404';
        }else{
            response.message = 'User Found';
            response.data = patientsList[0];
            response.status_code = '200';
        }                  
        return response;          
    }
    global class AuthenticationResponse{
        global String message;
        global String status_code;
        global Patient__c data;
    }
}