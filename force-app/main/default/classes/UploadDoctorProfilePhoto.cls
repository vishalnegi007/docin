public with sharing class UploadDoctorProfilePhoto {
    @AuraEnabled
    public static Contact getProfilePhoto(String doctorId) {
        List<Contact> doctor = new List<Contact>();
        doctor = [SELECT Id,LastName,Doctor_Photo__c,Doctor_Photo_ID__c FROM Contact WHERE Id =:doctorId LIMIT 1];

        if(!doctor.isEmpty()){
            return doctor[0];
        }else{
            return new Contact();
        }
    }
    @AuraEnabled
    public static Contact uploadDoctorProfilePhoto(String imageData,String doctorId) {
        String imageId ;
        String thumbnailLink;
        List<Contact> doctorProfile = new List<Contact>();
        doctorProfile = [SELECT Id,LastName,Doctor_Photo__c,Doctor_Photo_ID__c FROM Contact WHERE Id =:doctorId LIMIT 1];

        imageId = GoogleWebService.uploadFile(imageData,'doctor '+doctorId,doctorProfile[0].Doctor_Photo_ID__c);
        thumbnailLink = GoogleWebService.getFile(imageId);
       
        System.debug('thumbnailLink--> '+thumbnailLink);
        Contact doctor = new Contact();
        doctor.Id = doctorId;
        doctor.Doctor_Photo__c = thumbnailLink;
        doctor.Doctor_Photo_ID__c = imageId;
        update doctor;
        return doctor;
    }
    @AuraEnabled
    public static Contact removeProfilePhoto(String doctorId) {
        List<String> contactIds = new List<String>();
        contactIds.add(doctorId);
        ContactTriggerHandler.uploadDefaultImage(contactIds);
        return getProfilePhoto(doctorId);
    }
}