@RestResource(urlMapping='/getReviews/*')
global class GetReviewsWebservice{
    // Post Method
    @HTTPPost
    global static Response getAllReviews(){
        Response response = new Response();
        List<Rating__c> ratingList = new List<Rating__c>();
        try{
            RestRequest patientRequest = RestContext.request;
            // Getting the request body from the request
            String requestBody = patientRequest.requestBody.toString();
            // Deserializing the JSON response body and assigning it to an instance of Contact object
            Map<String,Object> params = (Map<String,Object>) JSON.deserializeUntyped(requestBody);
            
            
            if(!String.isBlank(params.get('userId').toString())){
                ratingList = [SELECT Id,Description__c,User__c,Ratings__c,Doctor__c FROM Rating__c WHERE User__c =:params.get('userId').toString()];
            }
            response.message = 'Ratings fetched Successfully';
            response.status_code = '200';
            response.data = ratingList;

        }catch(Exception ex){
            response.message = 'Error in getting Rating';
            response.status_code = '404';
            response.data = ratingList;
        }
        return response;
    }
    
    global class Response{
        public String message;
        public String status_code;
        public List<Rating__c> data;
    }
}