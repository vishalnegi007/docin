@isTest
public class ResetPassword_WwebserviceTest {
	@isTest
    public static void ResetPasswordTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"email":"test11@gmail.com"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/resetPassword';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	ResetPassword_Wwebservice.resetUserPassword();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
        
    }
    
    @isTest
    public static void ResetPasswordTest12(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"email":""}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/resetPassword';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	ResetPassword_Wwebservice.resetUserPassword();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
        
    }
    @isTest
    public static void ResetPasswordTest23(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        objPatien.Social_ID__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"email":}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/resetPassword';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	ResetPassword_Wwebservice.resetUserPassword();
        test.stopTest();
        List<Patient__c> lstPatien = [select id, name from Patient__c];
        System.assertEquals(lstPatien.size(),1);
        
    }
}