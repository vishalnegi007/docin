@isTest
public class TestAndProdceduresControllerTest {
    @testSetup static void initMethod() {
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Account';
        insert accountRecord;
        Test_Procedures__c testPro = new Test_Procedures__c();
        testPro.Existing_Patient_Fees__c = 120.00;
        testPro.New_Patient_Fees__c = 120.00;
        testPro.Procedure_Name__c = 'EKG';
        testPro.Procedure_Test__c = 'EKG';
        testPro.Service_Provider__c = accountRecord.Id;
        insert testPro;
    }
    @isTest static void testMethod1(){
        Test.startTest();
        Test_Procedures__c testPro = [SELECT Id, Existing_Patient_Fees__c, New_Patient_Fees__c, Procedure_Name__c, Procedure_Test__c, Service_Provider__c
                                      FROM Test_Procedures__c
                                      LIMIT 1];
        TestAndProdceduresController.getProcedureRecords(testPro.Id);
        TestAndProdceduresController.getProcedureRecords('invalidId');
        String procedureListString = '[{"Id":"a0E0q000001oFV4EAM","Existing_Patient_Fees__c":60,"New_Patient_Fees__c":60,"Procedure_Name__c":"Other","Procedure_Test__c":"test","Service_Provider__c":"0010q00000YxR4AAAV"},{"Id":"a0E0q000001oFdCEAU","Existing_Patient_Fees__c":0,"New_Patient_Fees__c":0,"Procedure_Name__c":"EKG","Procedure_Test__c":"EKG","Service_Provider__c":"0010q00000YxR4AAAV"}]';
        TestAndProdceduresController.saveAllProcedures(procedureListString);
        TestAndProdceduresController.saveAllProcedures('invalidJSON');
        TestAndProdceduresController.deleteProcedure(testPro.Id);
        TestAndProdceduresController.deleteProcedure(null);
        TestAndProdceduresController.getProceduresAndTestList();
        List<Test_Procedures__c> testProList = new List<Test_Procedures__c>([SELECT Id, Existing_Patient_Fees__c, New_Patient_Fees__c, Procedure_Name__c, Procedure_Test__c, Service_Provider__c
                                                                             FROM Test_Procedures__c
                                                                             WHERE Id =:testPro.Id
                                                                             LIMIT 1]);
        System.assertEquals(0, testProList.size());
        Test.stopTest();
    }
}