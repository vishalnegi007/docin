public without sharing class PaymentMethodsController {
    
    @AuraEnabled
    public static Map<String,Object> createPaymentMethod(String cardObjectString, Account accountRecord){
        Map<String, Object> cardObject = (Map<String, object>)JSON.deserializeUntyped(cardObjectString);
        Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
        Stripe_Credentials__c paymentMethodAPI = Stripe_Credentials__c.getValues('Create Payment Method');
        System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(paymentMethodAPI.Endpoint__c);
        request.setMethod('POST');    
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
        String payload = '';
        payload = 'type=card';
        payload += '&card[number]='+ EncodingUtil.urlEncode(cardObject.get('cardNumber').toString(),'UTF-8'); 
        payload += '&card[exp_month]='+ EncodingUtil.urlEncode(cardObject.get('cardExpMonth').toString(),'UTF-8'); 
        payload += '&card[exp_year]='+ EncodingUtil.urlEncode(cardObject.get('cardExpYear').toString(),'UTF-8');
        payload += '&card[cvc]='+ EncodingUtil.urlEncode(cardObject.get('cardCVC').toString(),'UTF-8');
        payload += '&billing_details[name]='+ EncodingUtil.urlEncode(accountRecord.Name,'UTF-8');
        if(String.isNotBlank(accountRecord.Email__c))
            payload += '&billing_details[email]='+ EncodingUtil.urlEncode(accountRecord.Email__c,'UTF-8');
        if(String.isNotBlank(accountRecord.Phone))
            payload += '&billing_details[phone]='+ EncodingUtil.urlEncode(accountRecord.Phone,'UTF-8');
        if(String.isNotBlank(accountRecord.BillingStreet))
            payload += '&billing_details[address][line1]='+ EncodingUtil.urlEncode(accountRecord.BillingStreet,'UTF-8');
        // payload += '&billing_details[address][line2]='+ EncodingUtil.urlEncode(accountRecord.Email__c,'UTF-8');
        if(String.isNotBlank(accountRecord.BillingCity))
            payload += '&billing_details[address][city]='+ EncodingUtil.urlEncode(accountRecord.BillingCity,'UTF-8');
        if(String.isNotBlank(accountRecord.BillingState))
            payload += '&billing_details[address][state]='+ EncodingUtil.urlEncode(accountRecord.BillingState,'UTF-8');
        // if(String.isNotBlank(accountRecord.BillingCountry))
        //     payload += '&billing_details[address][country]='+ EncodingUtil.urlEncode(accountRecord.BillingCountry,'UTF-8');
        if(String.isNotBlank(accountRecord.BillingPostaLCode))
            payload += '&billing_details[address][postal_code]='+ EncodingUtil.urlEncode(accountRecord.BillingPostaLCode,'UTF-8');
        System.debug(payload);
        
        request.setBody(payload);
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
            Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            return myMap;
        } else {
            Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            
            // Attaching Payment Method to the Account
            attachPaymentMethod(myMap.get('id').toString(), accountRecord.Stripe_Customer_Account__c);
            System.debug(response.getBody());
            return myMap;
        }
    }
    
    @AuraEnabled
    public static void attachPaymentMethod(String paymentMethodId, String customerId){
        Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
        Stripe_Credentials__c attachPaymentMethodAPI = Stripe_Credentials__c.getValues('Attach Payment Method');
        String endpoint = attachPaymentMethodAPI.Endpoint__c.replace('{methodId}',paymentMethodId);
        System.debug('endpoint--> '+endpoint);
        System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('POST');    
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
        String payload = 'customer='+EncodingUtil.urlEncode(customerId,'UTF-8');
        System.debug(payload);
        
        request.setBody(payload);
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
        } else {
            Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            System.debug(response.getBody());
        }
    }
    
    @AuraEnabled
    public static Map<String,Object> getAllPaymentMethods(String customerId){
        Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
        Stripe_Credentials__c paymentMethodAPI = Stripe_Credentials__c.getValues('Create Payment Method');
        System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
        String endpoint = paymentMethodAPI.Endpoint__c;
        endpoint += '?customer='+customerId+'&type=card';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpoint);
        request.setMethod('GET');    
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
        
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 200) {
            System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
            Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            return myMap;
        } else {
            Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
            return myMap;
            
        }
    }
    @AuraEnabled
    public static Map<String,Object> getCustomer(String customerId){
        try {
            Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
            Stripe_Credentials__c customerAPI = Stripe_Credentials__c.getValues('Create Customer');
            System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
            String endpoint = customerAPI.Endpoint__c+'/'+customerId;
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setMethod('GET');    
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
            
            HttpResponse response = http.send(request);
            // Parse the JSON response
            if (response.getStatusCode() != 200) {
                System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                return myMap;
            } else {
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                return myMap;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    @AuraEnabled
    public static Map<String,Object> deleteCreditCard(String paymentMethodId){
        try {
            Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
            Stripe_Credentials__c paymentMethodAPI = Stripe_Credentials__c.getValues('Create Payment Method');
            System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
            String endpoint = paymentMethodAPI.Endpoint__c+'/'+paymentMethodId+'/detach';
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setMethod('POST');    
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
            
            HttpResponse response = http.send(request);
            // Parse the JSON response
            if (response.getStatusCode() != 200) {
                System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                return myMap;
            } else {
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                System.debug(response.getBody());
                return myMap;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    @AuraEnabled
    public static Map<String,Object> setDefaultCreditCard(String paymentMethodId, String customerId){
        try {
            Stripe_Credentials__c stripeCredentials = Stripe_Credentials__c.getValues('Secret Key');
            Stripe_Credentials__c customerAPI = Stripe_Credentials__c.getValues('Create Customer');
            System.debug('stripeCredentials.Secret_Key__c--> '+stripeCredentials.Secret_Key__c);
            String endpoint = customerAPI.Endpoint__c+'/'+customerId;
            Http http = new Http();
            HttpRequest request = new HttpRequest();
            request.setEndpoint(endpoint);
            request.setMethod('POST');    
            request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            request.setHeader('Authorization','Bearer '+stripeCredentials.Secret_Key__c);
            String payload = '';
            payload = 'invoice_settings[default_payment_method]='+paymentMethodId;
            
            request.setBody(payload);
            HttpResponse response = http.send(request);
            // Parse the JSON response
            if (response.getStatusCode() != 200) {
                System.debug('The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getBody());
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                return myMap;
            } else {
                Map<String, Object> myMap = (Map<String, object>)JSON.deserializeUntyped(response.getBody());
                return myMap;
            }
        } catch (Exception e) {
            return null;
        }
    }
    
    @AuraEnabled
    public static Account getAccount(String recordId){
        
        Account accountRecord = [SELECT Id, Name, Email__c, Phone, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostaLCode,
                                 Stripe_Customer_Account__c FROM Account WHERE Id =:recordId LIMIT 1];
        return accountRecord;
    }
}