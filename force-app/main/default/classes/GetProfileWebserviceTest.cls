@isTest
public class GetProfileWebserviceTest {
	@isTest
    public static void GetProfileTest(){
        
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        insert objPatien;
        
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
		);
        insert u;
        String requestJson = '{"userId":"'+u.Id+'"}';
        //String requestJson = '{"doctorId":"4324ewdsadas","userId":"4324ewdsadas","rating":"1.2","details":"heart"}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getprofile';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	GetProfileWebservice.getUserProfile();
        test.stopTest();
        List<Patient__c> lstPatients = [select id, name from Patient__c];
        System.assertEquals(lstPatients.size(),1);
    }
    @isTest
    public static void GetProfileTest1(){
        
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        insert objPatien;
        
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
		);
        insert u;
        String requestJson = '{"userId":"'+objPatien.Id+'"}';
        //String requestJson = '{"doctorId":"4324ewdsadas","userId":"4324ewdsadas","rating":"1.2","details":"heart"}';
 		RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getprofile';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	GetProfileWebservice.getUserProfile();
        test.stopTest();
        List<Patient__c> lstPatients = [select id, name from Patient__c];
        System.assertEquals(lstPatients.size(),1);
    }
}