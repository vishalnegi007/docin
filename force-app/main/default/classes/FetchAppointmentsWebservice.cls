@RestResource(urlMapping='/getAppointments/*')
global with sharing class FetchAppointmentsWebservice {
    @HTTPPost
    global static Response getAllAppointments(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Response response = new Response();
        List<Appointment__c> appointmentsList = new List<Appointment__c>();
        try{
            Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            
            appointmentsList = [SELECT Id,Name,Appointment_Date__c,Appointment_Time__c,Patient_Date_of_Birth__c,Patient_Email__c,Patient_Name__c,Patient_Phone__c, Patient__c,Clinic__c,Clinic__r.Name, Patient__r.Name, Patient__r.Last_Name__c, Doctor__c,Doctor__r.Doctor_Photo_ID__c,Doctor__r.Doctor_Photo__c,Doctor__r.Title, Doctor__r.FirstName,Doctor__r.LastName
                                FROM Appointment__c 
                                WHERE Patient__c = :params.get('PatientId').toString()
                                LIMIT 10000];
            
            response.status_code = '200';
            response.message = 'Appointments Fetched Successfully';
            response.data = appointmentsList;
        }catch(Exception ex){
            response.status_code = '400';
            response.message = 'Error in fetching Appointments';
            response.data = appointmentsList;
        }
        return response;
    }
    global class Response{
        public String message;
        public String status_code;
        public List<Appointment__c> data;
    }
}