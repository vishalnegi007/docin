public without sharing class UpdateWorkingHoursController{

    @AuraEnabled
    public static String getUpdatedHours(String recordId){
        List<Account> accountRec = new List<Account>();
        accountRec = [SELECT Id,Closed_Days__c,Monday_Starting_Hours__c,Monday_Ending_Hours__c,Tuesday_Starting_Hours__c,Tuesday_Ending_Hours__c,Wednesday_Starting_Hours__c,Wednesday_Ending_Hours__c,Thursday_Starting_Hours__c,Thursday_Ending_Hours__c,
                     Friday_Starting_Hours__c,Friday_Ending_Hours__c,Saturday_Starting_Hours__c,Saturday_Ending_Hours__c,Sunday_Starting_Hours__c,Sunday_Ending_Hours__c
                     FROM Account
                     WHERE Id =:recordId
                     LIMIT 1];
        if(!accountRec.isEmpty()){
            return JSON.serialize(accountRec[0]);
        }else{
            return JSON.serialize(new Account());
        }
    }

    @AuraEnabled
    public static String updateAccount(String accountString){
        System.debug('accountRecord--->  '+accountString);
        Map<String,Object> accountMap = (Map<String,Object>)JSON.deserializeUntyped(accountString);
        System.debug('accountRecord-->? '+accountMap);
        Account accountRecord = new Account();
        accountRecord.Id = String.valueOf(accountMap.get('Id'));
        if(accountMap.containsKey('Closed_Days__c')){
            accountRecord.Closed_Days__c = String.valueOf(accountMap.get('Closed_Days__c'));
        }
        
        for(String key : accountMap.keySet()){
            
            if(key.contains('Hours__c') && accountMap.get(key) != null){
                String timeValue = String.valueOf(accountMap.get(key));
                System.debug('timeValue---> '+timeValue);
                accountRecord.put(key,Time.newInstance(Integer.valueOf(timeValue.split(':')[0]),Integer.valueOf(timeValue.split(':')[1]),00,000));
            }
        }
        
        System.debug('accountRecord--->  '+accountRecord);
        update accountRecord;
        return JSON.serialize(accountRecord);
    }
    
}