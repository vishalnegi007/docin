public class ContactTriggerHandler {
    public static void createDefaultImage(List<Contact> contactsList) {
        List<String> contactIds = new List<String>();
        for(Contact con : contactsList){
            contactIds.add(con.Id);
        }
        uploadDefaultImage(contactIds);
    }
    @future (callout=true)
    public static void uploadDefaultImage(List<String> contactIds){
        try{
            ContentVersion photoData = [Select Id,Title,VersionData FROM ContentVersion WHERE Title = 'Default Profile Image' LIMIT 1];
            String bodyEncoded = EncodingUtil.base64Encode(photoData.VersionData);
            List<Contact> doctorProfiles = new List<Contact>();
            List<Contact> doctorToUpdate = new List<Contact>();
            String imageId ;
            String thumbnailLink;
            doctorProfiles = [SELECT Id,LastName,Doctor_Photo__c,Doctor_Photo_ID__c FROM Contact WHERE Id IN :contactIds LIMIT 10000];
            for(Contact con : doctorProfiles){
                imageId = GoogleWebService.uploadFile(bodyEncoded,'doctor '+con.Id,con.Doctor_Photo_ID__c);
                thumbnailLink = GoogleWebService.getFile(imageId);
                
                System.debug('thumbnailLink--> '+thumbnailLink);
                con.Doctor_Photo__c = thumbnailLink;
                con.Doctor_Photo_ID__c = imageId;
            }
            update doctorProfiles;
        }catch(Exception ex){
            System.debug('Error at line --> '+ex.getLineNumber() + ' due to-----> '+ex.getMessage());
        }
    }
}