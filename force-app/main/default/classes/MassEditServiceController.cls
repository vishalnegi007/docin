public without sharing class MassEditServiceController {
    
    
    @AuraEnabled
    public static String deleteServiceRecord(String recId){
        try{
            Service_Provider__c service = new Service_Provider__c(Id=recId);
            delete service;
        } catch(Exception e) {
            throw new AuraHandledException(e.getMessage()+'-'+e.getStackTraceString());
        }
        
        return 'Success';
    }
    
    @AuraEnabled
    public static List<ServiceWithPrice> getServiceWithPriceInfo(String recId){
        
        
        List<ServiceWithPrice> serviceWithPriceList = new List<ServiceWithPrice>();
        
        List<Service_Provider__c>  serviceList = [SELECT Id,Visit_Type__c,Visit_Type_Name__c,
                                                  (SELECT Id,Existing_Patient__c,New_Patient__c,EKG__c FROM Price_List__r) 
                                                  FROM Service_Provider__c  WHERE Service_Provider__c=:recId ORDER BY CreatedDate ASC];
        
        
        for(Service_Provider__c obj : serviceList){
            
            ServiceWithPrice newWrap = new ServiceWithPrice();
            newWrap.ServiceId = obj.Id;
            newWrap.VisitType = obj.Visit_Type__c;
            newWrap.VisitTypeName = obj.Visit_Type_Name__c;
            
            for(Price_List__c priceObj : obj.Price_List__r){
                newWrap.PriceListId = priceObj.Id;
                newWrap.ExistingPatient = priceObj.Existing_Patient__c;
                newWrap.NewPatient = priceObj.New_Patient__c;
            }
            
            serviceWithPriceList.add(newWrap);
        }
        
        return serviceWithPriceList;
    }
    
    @AuraEnabled
    public static Map<String,String> getPicklistValue(String objectName,String fieldName){
        
        Map<String,String> valuesMap = new Map<String,String>();    
        
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            System.debug(pickListVal.getLabel() +' '+pickListVal.getValue());
            valuesMap.put(pickListVal.getValue(),pickListVal.getLabel());
        }
        
        return valuesMap;
    }
    
    @AuraEnabled
    public static String saveService(String recId,String serviceJson){
        
        try{
            
            List<ServiceWithPrice> serviceWithPriceList = new List<ServiceWithPrice>();
            serviceWithPriceList = (List<ServiceWithPrice>)JSON.deserialize(serviceJson,List<ServiceWithPrice>.class);
            Map<Integer,Service_Provider__c> serviceMap = new Map<Integer,Service_Provider__c>();
            Map<Integer,Price_List__c> priceMap = new Map<Integer,Price_List__c>();
            
            
            Integer i=0;
            
            for(ServiceWithPrice obj : serviceWithPriceList ){
                
                Service_Provider__c serviceRecord = new Service_Provider__c();
                if(String.isNotBlank(obj.ServiceId)){
                    serviceRecord.Id= obj.ServiceId;
                } else{
                    serviceRecord.Service_Provider__c = recId;
                    
                }    

                serviceRecord.Visit_Type__c = obj.VisitType;
                serviceRecord.Visit_Type_Name__c = obj.VisitTypeName;
                
                serviceMap.put(i,serviceRecord);
                
                Price_List__c priceObj = new Price_List__c ();
                if(String.isNotBlank(obj.PriceListId)){
                    priceObj.Id= obj.PriceListId;
                }
                priceObj.Existing_Patient__c = obj.ExistingPatient;
                priceObj.New_Patient__c = obj.NewPatient;
                
                
                priceMap.put(i,priceObj);
                
                i++;
                
            }
            
            if(!serviceMap.IsEmpty()){
                upsert serviceMap.values();
                
                for(Integer index : priceMap.Keyset()){
                    
                    Price_List__c priceObj = priceMap.get(index);
                    if(priceObj.Id == null && priceObj.Service_Provider__c == null){
                        priceObj.Service_Provider__c = serviceMap.get(index).Id;
                    }
                    
                    priceMap.put(index,priceObj);
                }
                
                upsert priceMap.values();
                
            }
            
            
        }  catch(Exception e)
        {
            System.debug('MSG-->'+e.getMessage()+'-'+e.getLineNumber());
            throw new AuraHandledException(e.getMessage()+'-'+e.getStackTraceString());
        }
        return 'Success';
    } 
    
    @AuraEnabled(cacheable=true)
    public static List <customValueWrapper> getContactList(String recId) {
        
        list < customValueWrapper > customObjWrapper = new list < customValueWrapper > ();
        for (Contact myCustPick: [SELECT Id,Name,AccountId FROM Contact WHERE AccountId =:recId]) {
            
            customValueWrapper selectOptionValueWrapper = new customValueWrapper();
            selectOptionValueWrapper.label = myCustPick.Name;
            selectOptionValueWrapper.value = myCustPick.Id;
            customObjWrapper.add(selectOptionValueWrapper);
        }
        
        return customObjWrapper;
        
    }
    // wrapper class 
    public with sharing class customValueWrapper {
        @auraEnabled public string label {get;set;}
        @auraEnabled public string value {get;set;}
    }
    
    
    public class ServiceWithPrice{
        
        @AuraEnabled
        public string ServiceId;
        @AuraEnabled
        public string VisitType;
        @AuraEnabled
        public string VisitTypeName;
        @AuraEnabled
        public string PriceListId;
        @AuraEnabled
        public Decimal ExistingPatient;
        @AuraEnabled
        public Decimal NewPatient;
        
        
    }
}