@isTest
public class ResetPassword_DOCIN_ControllerTest {
    @testSetup static void setup() {
        // Create common test accounts
        List<Patient__c> testPatientsList = new List<Patient__c>();
        for(Integer i=0;i<2;i++) {
            Patient__c patient = new Patient__c();
            patient.Name = 'Test';
            patient.Last_Name__c = 'LastName';
            patient.Email__c = 'test@testmail.com';
            patient.Username__c = 'testUsername'+i;
            patient.Password__c = 'password'+1;
            testPatientsList.add(patient);
        }
        insert testPatientsList;        
    }
    @isTest static void userPasswordTest() {
        Patient__c patient = [SELECT Id,Name FROM Patient__c LIMIT 1];
        ResetPassword_DOCIN_Controller.resetUserPassword(patient.Id,'newPassword');
        ResetPassword_DOCIN_Controller.sendForgotPasswordEmail('testUsername1');
    }
}