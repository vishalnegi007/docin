@RestResource(urlMapping='/getprofile/*')
global class GetProfileWebservice {
    @HTTPPost
    global static Response getUserProfile() {
        Response response = new Response();
        Patient__c profile = new Patient__c();
        try{
            RestRequest req = RestContext.request;
            Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            
            if(String.isNotBlank(params.get('userId').toString())){
                profile = [SELECT Id,Email__c,Date_of_Birth__c,Name,Last_Name__c,Gender__c,Password__c,Phone__c,Username__c,User_Image__c,Profile_Photo_ID__c,
                           (SELECT Id,User__c,Description__c,Doctor__r.Doctor_Photo_ID__c,Doctor__r.Doctor_Photo__c,Doctor__c,Doctor__r.Title,Doctor__r.FirstName,Doctor__r.LastName,Ratings__c FROM Ratings__r)
                           FROM Patient__c
                           WHERE Id = :params.get('userId').toString()
                           LIMIT 1];
                    }
            
            response.status_code = '200';
            response.message = 'Profile fetched successfully';
            response.data = profile;
        }catch(Exception ex){
            response.status_code = '400';
            response.message = 'Error in fetching profile';
            response.data = profile;
        }
        return response;
    }
    
    global class Response{
        public String message;
        public String status_code;
        public Patient__c data;
    }
}