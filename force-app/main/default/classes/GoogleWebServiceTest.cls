@isTest
public class GoogleWebServiceTest {

    @isTest static void createFolderTest(){
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        GoogleWebService.createFolder('Test Folder Name');
        GoogleWebService.getFile('fileId123');
        GoogleWebService.uploadFile('TestImageData', 'TestUserId', 'fileId123');
        GoogleWebService.uploadFile('TestImageData', 'TestUserId', null);
        GoogleWebService.uploadFileInFolder('TestImageData', 'TestUserId', 'fileId123','folderId');
    }
}