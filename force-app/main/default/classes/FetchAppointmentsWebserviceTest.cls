@isTest
public class FetchAppointmentsWebserviceTest {
	@isTest
    public static void FetchAppointmentsTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        insert objPatien;
        
        Appointment__c objAppointment = new Appointment__c();
        objAppointment.Patient__c = objPatien.Id;
        insert objAppointment;
        
        String requestJson = '{"Patient__c":"'+objPatien.Id+'"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getAppointments';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	FetchAppointmentsWebservice.getAllAppointments();
        test.stopTest();
        List<Appointment__c> lstAppoint = [select id, name from Appointment__c];
        System.assertEquals(lstAppoint.size(),1);
        
    }
    @isTest
    public static void FetchAppointmentsTest1(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        insert objPatien;
        
        Appointment__c objAppointment = new Appointment__c();
        objAppointment.Patient__c = objPatien.Id;
        insert objAppointment;
        
        String requestJson = '{"PatientId":"'+objPatien.Id+'"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getAppointments';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	FetchAppointmentsWebservice.getAllAppointments();
        test.stopTest();
        List<Appointment__c> lstAppoint = [select id, name from Appointment__c];
        System.assertEquals(lstAppoint.size(),1);
        
    }
}