@RestResource(urlMapping='/updateProfile/*')
global class UpdateProfile_Weservice {
    @HTTPPost
    global static Response updateProfile(){
        Response response = new Response();
        List<Patient__c> userRecord = new List<Patient__c>();
        try{
            RestRequest req = RestContext.request;
            Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            String userId = params.get('userId').toString();
            List<Patient__c> patientRecord = new List<Patient__c>();
            patientRecord = [SELECT Id,Date_of_Birth__c,Email__c,Name,Last_Name__c,Gender__c,Password__c,Phone__c,Username__c,User_Image__c,Profile_Photo_ID__c
                             FROM Patient__c
                             WHERE Id = :userId
                             LIMIT 1];
            if(!patientRecord.isEmpty()){
                if(String.isNotBlank(params.get('firstName').toString()))
                    patientRecord[0].Name = params.get('firstName').toString();
                if(String.isNotBlank(params.get('lastName').toString()))
                    patientRecord[0].Last_Name__c = params.get('lastName').toString();
                if(String.isNotBlank(params.get('email').toString()))
                    patientRecord[0].Email__c = params.get('email').toString();
                if(String.isNotBlank(params.get('phone').toString()))
                    patientRecord[0].Phone__c = params.get('phone').toString();
                if(String.isNotBlank(params.get('gender').toString()))
                    patientRecord[0].Gender__c = params.get('gender').toString();
                if(String.isNotBlank(params.get('dateOfBirth').toString()))
                    patientRecord[0].Date_of_Birth__c = Date.valueOf(params.get('dateOfBirth').toString());
                
                //Uploading Photo on Google Drive
                String imageId;
                String thumbnailLink;
                if(params.containsKey('userimage') && String.isNotBlank(params.get('userimage').toString())){
                    imageId = GoogleWebService.uploadFile(params.get('userimage').toString(),userId,patientRecord[0].Profile_Photo_ID__c);
                    
                    if(imageId!=null && String.isNotBlank(imageId)){
                        thumbnailLink = GoogleWebService.getFile(imageId);
                        
                        
                        System.debug('imageId--> '+imageId);
                        patientRecord[0].Profile_Photo_ID__c = imageId;
                        patientRecord[0].User_Image__c = thumbnailLink;
                    }
                }
                update patientRecord[0];
                
                response.status_code = '200';
                response.message = 'Profile Updated Successfully';
                response.data = patientRecord[0];
            }else{
                response.status_code = '400';
                response.message = 'Error in updating profile';
            }
        }catch(Exception ex){
            response.status_code = '400';
            response.message = 'Error in updating profile';
        }
        return response;
    }
    global class Response{
        global String message;
        global String status_code;
        global Patient__c data;
    }
}