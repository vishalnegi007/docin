@isTest
public class AuthenticateLoginWebserviceTest {
	@isTest
    public static void AuthenticateLoginTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"Username":"Test patient17666","Password":"1234567890"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/authLogin';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	AuthenticateLoginWebservice.authenticatePatient();
        test.stopTest();
        List<Patient__c> lstPatients = [select id, name from Patient__c];
        System.assertEquals(lstPatients.size(),1);
    }
    
    @isTest
    public static void AuthenticateLoginTest123(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        objPatien.Password__c = '1234567890';
        insert objPatien;
        
        String requestJson = '{"Username":"","Password":""}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/authLogin';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	AuthenticateLoginWebservice.authenticatePatient();
        test.stopTest();
        List<Patient__c> lstPatients = [select id, name from Patient__c];
        System.assertEquals(lstPatients.size(),1);
    }
}