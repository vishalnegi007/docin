/**
* An apex page controller that supports self registration of users in communities that allow self registration
*/
@IsTest public with sharing class CommunitiesSelfRegControllerTest { 
    @testSetup static void initMethod() {
        List<Stripe_Credentials__c> credentials = new List<Stripe_Credentials__c>();
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        credentials.add(stripeCredentail);
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        credentials.add(stripeCredentail1);
        
        Stripe_Credentials__c stripeCredentail2 = New Stripe_Credentials__c();
        stripeCredentail2.Name = 'Create Payment Method';
        stripeCredentail2.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail2.EndPoint__c = 'www.testEndpoint.com';
        credentials.add(stripeCredentail2);
        
        Stripe_Credentials__c stripeCredentail3 = New Stripe_Credentials__c();
        stripeCredentail3.Name = 'Attach Payment Method';
        stripeCredentail3.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail3.EndPoint__c = 'www.testEndpoint.com{methodId}';
        credentials.add(stripeCredentail3);
        insert credentials;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Account';
        insert accountRecord;
        
        Subscription_Plan__c plan = new Subscription_Plan__c();
        plan.Name = 'Monthly';
        plan.Plan_Charges__c = 120;
        insert plan;
        
    }
    @isTest static void testCommunitiesSelfRegController() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.firstName = 'FirstName';
        controller.lastName = 'LastName';
        controller.email = 'test@force.com';
        controller.communityNickname = 'test';
        controller.accountName = 'Test Account';
        controller.accountBillingStreet = 'Test';
        controller.accountBillingCity = 'Test';
        controller.accountBillingState = 'Test';
        controller.accountBillingPostalCode = 'Test';
        controller.accountBillingCountry = 'Test';
        controller.password = 'Test';
        controller.confirmPassword = 'Test';
        System.assert(controller.registerUser() == null);  
        Test.stopTest();
    }    
    @isTest static void testCommunitiesSelfRegController1() {
        CommunitiesSelfRegController controller = new CommunitiesSelfRegController();
        controller.confirmPassword = 'Test';
        
        
        // registerUser will always return null when the page isn't accessed as a guest user
        System.assert(controller.registerUser() == null);    
        
        controller.password = 'abcd1234';
        controller.confirmPassword = 'abcd123';
        System.assert(controller.registerUser() == null);
        Account accountRecord = [SELECT Id,Name FROM Account LIMIT 1];
        String cardString = '{"cardNumber":"4111111111111111","cardExpMonth":"04","cardExpYear":"2024","cardCVC":"123"}';
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        CommunitiesSelfRegController.createPaymentMethod(cardString,accountRecord);
        CommunitiesSelfRegController.getAccount(accountRecord.Id);
        CommunitiesSelfRegController.loginCommunity('abc','abc');
        CommunitiesSelfRegController.getSubscriptionPlans();
        Subscription_Plan__c plan = [SELECT Id,Name FROM Subscription_Plan__c LIMIT 1];
        CommunitiesSelfRegController.updateAcount(plan.Id,accountRecord);
    }    
}