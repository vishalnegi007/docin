public without sharing class UploadClinicImagesController {
   @AuraEnabled
    public static ImageWrapper getClinicImages(String accountId){
        ImageWrapper imageWrapper = new ImageWrapper();
        
        Account accountRecord = [SELECT Id,Name,Google_Image_Folder_id__c,Clinic_Image_1__c,Clinic_Image_1_Id__c,Clinic_Image_2__c,Clinic_Image_2_Id__c,Clinic_Image_3__c,Clinic_Image_3_Id__c,Clinic_Image_4__c,Clinic_Image_4_Id__c,Clinic_Image_5__c,Clinic_Image_5_Id__c
        FROM Account
        WHERE ID =:accountId
        LIMIT 1];
        imageWrapper.accountRecord = accountRecord;

        if(accountRecord.Clinic_Image_1__c!=null && accountRecord.Clinic_Image_1_Id__c!=null){
            imageWrapper.image1 = accountRecord.Clinic_Image_1__c;
            imageWrapper.image1_Id = accountRecord.Clinic_Image_1_Id__c;
        }
        if(accountRecord.Clinic_Image_2__c!=null && accountRecord.Clinic_Image_2_Id__c!=null){
            imageWrapper.image2 = accountRecord.Clinic_Image_2__c;
            imageWrapper.image2_Id = accountRecord.Clinic_Image_2_Id__c;
        }
        if(accountRecord.Clinic_Image_3__c!=null && accountRecord.Clinic_Image_3_Id__c!=null){
            imageWrapper.image3 = accountRecord.Clinic_Image_3__c;
            imageWrapper.image3_Id = accountRecord.Clinic_Image_3_Id__c;
        }
        if(accountRecord.Clinic_Image_4__c!=null && accountRecord.Clinic_Image_4_Id__c!=null){
            imageWrapper.image4 = accountRecord.Clinic_Image_4__c;
            imageWrapper.image4_Id = accountRecord.Clinic_Image_4_Id__c;
        }
        if(accountRecord.Clinic_Image_5__c!=null && accountRecord.Clinic_Image_5_Id__c!=null){
            imageWrapper.image5 = accountRecord.Clinic_Image_5__c;
            imageWrapper.image5_Id = accountRecord.Clinic_Image_5_Id__c;
        }


        return imageWrapper;
    }
    @AuraEnabled
    public static Boolean uploadClinicImages(List<String> filesList,String accountRecord){
        Account accountRec = (Account) JSON.deserialize(accountRecord, Account.class);
        System.debug('accountRec-->? '+accountRec);
        for(Integer i=0;i<filesList.size();i++){
            System.debug('file '+i+' -->'+filesList[i]);
        }
        String folderId = '';
        if(accountRec.Google_Image_Folder_id__c!=null){
            folderId = accountRec.Google_Image_Folder_id__c;
        }else{
            folderId = GoogleWebService.createFolder(accountRec.Name);
        }
        accountRec.Google_Image_Folder_id__c = folderId;

        List<String> imagesId = new List<String>();
        Map<String,String> fieldAndImageIdMap = new Map<String,String>();
        Map<String,String> fieldAndImageLinkMap = new Map<String,String>();
        for(Integer i=0;i<filesList.size();i++){
            String photoField = 'Clinic_Image_'+(i+1)+'__c';
            String idField = 'Clinic_Image_'+(i+1)+'_Id__c';
            String photoId = String.valueOf(accountRec.get(idField)) != 'null' ? String.valueOf(accountRec.get(idField)) : null;

            String imageId = GoogleWebService.uploadFileInFolder(filesList[i],photoField,photoId,folderId);
            String thumbnailLink = GoogleWebservice.getFile(imageId);

            fieldAndImageIdMap.put(idField,imageId);
            fieldAndImageLinkMap.put(photoField,thumbnailLink);

            accountRec.put(idField, imageId);
            accountRec.put(photoField, thumbnailLink);
        }

        System.debug('accountRec--> '+accountRec);

        update accountRec;
        return true;
    }
    public class ImageWrapper{
        @AuraEnabled public String image1;
        @AuraEnabled public String image1_Id;
        @AuraEnabled public String image2;
        @AuraEnabled public String image2_Id;
        @AuraEnabled public String image3;
        @AuraEnabled public String image3_Id;
        @AuraEnabled public String image4;
        @AuraEnabled public String image4_Id;
        @AuraEnabled public String image5;
        @AuraEnabled public String image5_Id;
        @AuraEnabled public Account accountRecord;
    }
}