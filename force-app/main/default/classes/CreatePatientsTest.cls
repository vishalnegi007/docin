@isTest
public class CreatePatientsTest {
    @isTest static void insertPatientDataTest(){
        Patient__c patient = new Patient__c();
        patient.Name = 'TEST';
        patient.Last_Name__c = 'TEST';
        patient.Email__c = 'TEST@TEST.com';
        patient.Gender__c = 'Male';
        patient.Username__c = 'TEST';
        patient.Password__c = 'TEST';
        
        CreatePatients.insertPatientData(JSON.serialize(patient));
    }
}