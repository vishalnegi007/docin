@isTest
public class BookAnAppointmentWebserviceTest {
	@isTest
    public static void BookAnAppointmentTest(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        insert objPatien;
        
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        Account objAccount = new Account();
        objAccount.Name = 'clinic1';
        objAccount.BillingPostalCode = '1234567';
        objAccount.Phone = '9329423949';
        objAccount.Email__c = 'testnew@gmail.com';
        insert objAccount;
        
        Contact businessContact = new Contact();
        businessContact.AccountId = objAccount.Id;
        businessContact.LastName = 'Apex';
        businessContact.FirstName = 'Test';
        insert businessContact;
        
        String requestJson = '{"doctorId":"'+businessContact.Id+'","patientId":"'+objPatien.Id+'","email":"test123@gmail.com","phone":"1234567890","date":"2021-05-01","time":"08:25","name":"Test Appointment12"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/createAppointment';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	BookAnAppointmentWebservice.createAppointmentRecord();
        test.stopTest();
        List<Appointment__c> lstAppointments = [select id, name from Appointment__c];
        System.assertEquals(lstAppointments.size(),1);
    }
    @isTest
    public static void BookAnAppointmentTest12(){
        Patient__c objPatien = new Patient__c();
        objPatien.Email__c = 'test11@gmail.com';
        objPatien.Name = 'Test patient';
        objPatien.Last_Name__c = 'Test patient123';
        objPatien.Username__c = 'Test patient17666';
        insert objPatien;
        
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        Account objAccount = new Account();
        objAccount.Name = 'clinic1';
        objAccount.BillingPostalCode = '1234567';
        objAccount.Phone = '9329423949';
        objAccount.Email__c = 'testnew@gmail.com';
        insert objAccount;
        
        Contact businessContact = new Contact();
        businessContact.AccountId = objAccount.Id;
        businessContact.LastName = 'Apex';
        businessContact.FirstName = 'Test';
        insert businessContact;
        
        String requestJson = '{"doctorId":,"patientId":"'+objPatien.Id+'","email":"test123@gmail.com","phone":"1234567890","date":"2021-05-01","time":"08:25","name":"Test Appointment12"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/createAppointment';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	BookAnAppointmentWebservice.createAppointmentRecord();
        test.stopTest();
        List<Appointment__c> lstAppointments = [select id, name from Appointment__c];
        System.assertEquals(lstAppointments.size(),0);
    }
}