/**
* An apex page controller that supports self registration of users in communities that allow self registration
*/
public class CommunitiesSelfRegController {
    public String accountName {get; set;}
    public String accountBillingStreet {get; set;}
    public String accountBillingCity {get; set;}
    public String accountBillingState {get; set;}
    public String accountBillingPostalCode {get; set;}
    public String accountBillingCountry {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String account_Id {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public Boolean displaySubscriptionPlanPage {get; set;}
    public Boolean displaySignUpPage {get; set;}
    public String accountRecordId {get; set;}

    public static String userEmail;
    public static String userpassword;
    
    public String userId{get; set;}
    public String user_Email{get; set;}
    public String user_Password{get; set;}
    
    public CommunitiesSelfRegController() {        
        displaySubscriptionPlanPage = false;
        displaySignUpPage = true;
        String expid = ApexPages.currentPage().getParameters().get('expid');            
        if (expId != null) {
            Site.setExperienceId(expId); 
        }    
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public void registerUser() {
        // Create a savepoint while AccountNumber is null
        Savepoint sp;
        if(!Test.isRunningTest())
            sp = Database.setSavepoint();
        try{
                List<string> errorMessageList = New List<string>(getErrors());
            
            System.debug('errorMessageList:::'+errorMessageList);
            if (errorMessageList.size() > 0) {
                for (String msg : errorMessageList) {
                    ApexPages.Message erMsg = new ApexPages.Message(ApexPages.Severity.FATAL, msg);
                    ApexPages.addMessage(erMsg);
                }
            }
            else {
                Account acc = New Account();
                acc.Name = accountName;
                acc.BillingStreet = accountBillingStreet;
                acc.BillingCity = accountBillingCity;
                acc.BillingState = accountBillingState;
                acc.BillingPostalCode = accountBillingPostalCode;
                acc.BillingCountry = accountBillingCountry;
                
                insert acc;
                System.debug('acc.Id::');
                accountRecordId = acc.Id;
                
                
                User adminUser = [SELECT ID,FirstName FROM User WHERE Profile.Name='System Administrator' AND IsActive = true LIMIT 1];
                acc.isPartner = true;
                acc.OwnerId = adminUser.Id;
                update acc;
                system.debug('AccountId::'+acc.Id +' :: '+ acc.Name);
                
                List<UserRole> userRole = [SELECT ID,Name FROM UserRole WHERE DeveloperName = 'CEO' limit 1];
                List<Profile> profile = [SELECT ID,Name FROM Profile WHERE Name = 'Partner Community User Copy' limit 1];
                String profileId = profile[0].Id; // To be filled in by customer.
                String roleEnum = userRole[0].Id; // To be filled in by customer.
                String accountId = acc.Id; // To be filled in by customer.
                account_Id = acc.Id;
                String userName = email;
                
                
                User u = new User();
                u.Username = userName;
                u.Email = email;
                u.FirstName = firstName;
                u.LastName = lastName;
                u.CommunityNickname = userName;
                u.ProfileId = profileId;
                
                try {
                    userId = Site.createExternalUser(u, accountId, password);
                    //assignPermissionSet(userId);
                } catch(Site.ExternalUserCreateException ex) {
                    // Rollback to the previous null value
                    if(!Test.isRunningTest())
                        Database.rollback(sp);
                    List<String> errors = ex.getDisplayMessages();
                    System.debug('errors:::'+errors);
                    if(ex.getMessage().containsIgnoreCase('User already exists.')){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Email/Username already registered'));
                    }else{
                        for (String error : errors)  {
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
                        }
                    }
                    // This message is used for debugging. Do not display this in the UI to the end user.
                    // It has the information around why the user creation failed.
                    System.debug(ex.getMessage());
                }
                userEmail = email;
                user_Email = email;
                userPassword = password;
                user_Password = password;
                System.debug('userId--> '+userId);
                if (userId != null) { 
                    AccountShare accountSharing = new AccountShare();
                    accountSharing.AccountAccessLevel = 'Edit';
                    accountSharing.OpportunityAccessLevel='Read'; //Access Level
                    accountSharing.UserOrGroupId = userId;//UserId or Public Group Id
                    accountSharing.AccountId = acc.Id;//Object Record Id
                    accountSharing.RowCause = 'Manual';//Record Sharing Reason
                    Insert accountSharing;
                    if (password != null && password.length() > 1) {
                        displaySubscriptionPlanPage = true;
                        displaySignUpPage = false;
                    }
                }
            }
        }catch(Exception ex){
            System.debug('Error at Line--> '+ex.getLineNumber() +' due to ---> '+ex.getMessage());
            if(!Test.isRunningTest())
                Database.rollback(sp);
            if(ex.getMessage().containsIgnoreCase('duplicate record')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Practice name already registered. Please use a diffrent practice name'));
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There is a internal issue in signing up. Please try again after sometime.'));
            }
            
        }
    }
    
    Public List<string> getErrors() {
        List<string> errorList = New List<string>();
        
        if (String.isBlank(accountName) || String.isEmpty(accountName)) {
            errorList.add('Practice/Clinic Name is required to continue.');
        }
        
        if (String.isBlank(accountBillingStreet) || String.isEmpty(accountBillingStreet)) {
            errorList.add('Street is required to continue.');
        }
        
        if (String.isBlank(accountBillingCity) || String.isEmpty(accountBillingCity)) {
            errorList.add('City is required to continue.');
        }
        
        if (String.isBlank(accountBillingState) || String.isEmpty(accountBillingState)) {
            errorList.add('State is required to continue.');
        }
        
        if (String.isBlank(accountBillingPostalCode) || String.isEmpty(accountBillingPostalCode)) {
            errorList.add('Zip/Postal Code is required to continue.');
        }
        
        if (String.isBlank(accountBillingCountry) || String.isEmpty(accountBillingCountry)) {
            errorList.add('Country is required to continue.');
        }
        
        if (String.isBlank(firstName) || String.isEmpty(firstName)) {
            errorList.add('First Name is required to continue.');
        }
        
        if (String.isBlank(lastName) || String.isEmpty(lastName)) {
            errorList.add('Last Name is required to continue.');
        }
        
        // if (String.isBlank(communityNickname) || String.isEmpty(communityNickname)) {
        //     errorList.add('Username is required to continue.');
        // }
        
        if (String.isBlank(email) || String.isEmpty(email)) {
            errorList.add('Company Email is required to continue.');
        }
        
        if (String.isBlank(password) || String.isEmpty(password)) {
            errorList.add('Password is required to continue.');
        }
        
        if (String.isBlank(confirmPassword) || String.isEmpty(confirmPassword)) {
            errorList.add('Confirm Password is required to continue.');
        }
        
        // it's okay if password is null - we'll send the user a random password in that case
        if ((String.isNotBlank(password) || String.isNotEmpty(password)) && (String.isNotBlank(confirmPassword) || String.isNotEmpty(confirmPassword)) && !isValidPassword()) {
            errorList.add(Label.site.passwords_dont_match);
        }
        
        return errorList;
    }
    
    // @AuraEnabled
    // public static void assignPermissionSet(String username){
    //     List<User> userList = New List<User>([SELECT Id FROM User where username=:username]);
    //     if(!userList.isEmpty()){
    //         List<PermissionSet> permissionSetList = [SELECT ID FROM PermissionSet where name='Cash2Care_Permission_Set' limit 1];
    //         System.debug('permissionSetList::'+permissionSetList);
    //         PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permissionSetList[0].id, AssigneeId = userList[0].Id);
    //         insert psa;
    //         System.debug('psa::'+psa);
    //     }
    // }
    
    @AuraEnabled
  public static Map<String,Object> createPaymentMethod(String cardObjectString, String accountString){
    Account accountRecord = (Account)System.JSON.deserialize(accountString, Account.class);
    Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    List<String> accountFields = new List<String>();
    Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
    for(Schema.SObjectField sfield : fieldMap.Values()){
        schema.describefieldresult dfield = sfield.getDescribe();
        accountFields.add(dField.getName());
    }
    String accId = accountRecord.Id;
    String accountQuery = 'SELECT '+ String.join(accountFields, ',') +' FROM Account WHERE Id=:accId LIMIT 1';
    List<Account> accountList = Database.query(accountQuery);
    Map<String,Object> paymentResponse = PaymentMethodsController.createPaymentMethod(cardObjectString,accountList[0]);
    return paymentResponse;
  }
    @AuraEnabled
    public static Account getAccount(String accountId){
       
        String accId = '\''+accountId+'\'';	
        String query = 'SELECT Id, Name FROM Account WHERE Id ='+accId+' LIMIT 1';
      
        Account accountRecord = Database.query(query);
        return accountRecord;
        
    }
    @AuraEnabled
    public static string loginCommunity(String username, String password){
        try {
            ApexPages.PageReference lgn = Site.login(username,password, '');
            aura.redirect(lgn);
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    
    @AuraEnabled
    public static string updateAcount(String subscriptionPlanId, Account accountRecord){
        accountRecord.Subscription_Plan__c = subscriptionPlanId;
        update accountRecord;
        return null;
    }
    
    @AuraEnabled
    public static List<Subscription_Plan__c> getSubscriptionPlans(){
        try {
            List<Subscription_Plan__c> allSubscriptions = new List<Subscription_Plan__c>();
            allSubscriptions = [SELECT Id,Name,Plan_Charges__c FROM Subscription_Plan__c ORDER BY Plan_Charges__c ASC LIMIT 100];
            return allSubscriptions;
        } catch (Exception e) {
            return null;
        }
    }

    public class RegistrationWrapper{
        @AuraEnabled
        public Boolean isValid;
        @AuraEnabled
        public String message;
        @AuraEnabled
        public String userObject;
    }
    public class UserWrapper{
        public String practiceName;	//
        public String firstName;	//
        public String lastName;	//
        public String userName;	//
        public String email;	//
        public String password;	//
        public String confirmPassword;	//
    }
}