@isTest
public class UploadDoctorProfilePhotoTest {
    @testSetup static void initMethod() {
        Contact contactRecord = new Contact();
        contactRecord.FirstName = 'Test';
        contactRecord.LastName = 'Test';
        contactRecord.Doctor_Photo__c = 'TEST';
        contactRecord.Doctor_Photo_ID__c = 'TEST';
        insert contactRecord;
    }
    @isTest static void uploadDoctorProfilePhotoTest(){
        Test.startTest();
        Contact contactRecord = [SELECT Id,LastName,Doctor_Photo__c,Doctor_Photo_ID__c FROM Contact LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        UploadDoctorProfilePhoto.getProfilePhoto(contactRecord.Id);
        UploadDoctorProfilePhoto.uploadDoctorProfilePhoto('TESTIMAGEDATA',contactRecord.Id);
        Test.stopTest();
    }
}