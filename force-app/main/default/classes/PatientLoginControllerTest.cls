@isTest 
public class PatientLoginControllerTest {
    @isTest static void checkLoginAuth() {
        Patient__c patientRecord = new Patient__c();
        patientRecord.Name = 'Test';
        patientRecord.Last_Name__c = 'Test';
        patientRecord.Email__c = 'Test@TestEmail.com';
        patientRecord.Gender__c = 'Male';
        patientRecord.Phone__c = '9810455322';
        patientRecord.Username__c = 'Test';
        patientRecord.Password__c = 'Test';
        insert patientRecord;

        PatientLoginController.checkLoginAuth(patientRecord.Username__c,patientRecord.Password__c);
    }
}