@isTest
public class ContactTriggerHandlerTest {
    @isTest
    public static void createDefaultImageTest() {
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
		ContentVersion contentVersion_1 = new ContentVersion();
        contentVersion_1.Title='Default Profile Image'; 
        contentVersion_1.PathOnClient ='SampleTitle.jpg';
        contentVersion_1.VersionData = Blob.valueOf('TEST DATA'); 
        contentVersion_1.origin = 'H';
        insert contentVersion_1;
        Test.startTest();
        Contact con = new Contact();
        con.FirstName = 'TEST';
        con.LastName = 'TEST';
        insert con;
        Test.stopTest();
    }
}