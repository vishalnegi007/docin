@isTest
public class UploadClinicImagesControllerTest {
    @testSetup static void initMethod() {
        List<Stripe_Credentials__c> credentials = new List<Stripe_Credentials__c>();
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        credentials.add(stripeCredentail);
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        credentials.add(stripeCredentail1);
        
        Stripe_Credentials__c stripeCredentail2 = New Stripe_Credentials__c();
        stripeCredentail2.Name = 'Create Payment Method';
        stripeCredentail2.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail2.EndPoint__c = 'www.testEndpoint.com';
        credentials.add(stripeCredentail2);
        
        Stripe_Credentials__c stripeCredentail3 = New Stripe_Credentials__c();
        stripeCredentail3.Name = 'Attach Payment Method';
        stripeCredentail3.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail3.EndPoint__c = 'www.testEndpoint.com{methodId}';
        credentials.add(stripeCredentail3);
        insert credentials;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Account';
        accountRecord.Clinic_Image_1__c = 'Test';
        accountRecord.Clinic_Image_1_Id__c = 'TEST';
        accountRecord.Clinic_Image_2__c = 'TEST';
        accountRecord.Clinic_Image_2_Id__c = 'TEST';
        accountRecord.Clinic_Image_3__c = 'TEST';
        accountRecord.Clinic_Image_3_Id__c = 'TEST';
        accountRecord.Clinic_Image_4__c = 'TEST';
        accountRecord.Clinic_Image_4_Id__c = 'TEST';
        accountRecord.Clinic_Image_5__c = 'TEST';
        accountRecord.Clinic_Image_5_Id__c = 'TEST';
        insert accountRecord;
    }
    @isTest static void uploadClinicImagesTest(){
        Account accountRecord = [SELECT Id,Name,Clinic_Image_1__c,Clinic_Image_2__c,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostaLCode,Stripe_Customer_Account__c FROM Account LIMIT 1];
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        UploadClinicImagesController.getClinicImages(accountRecord.Id);
        
        List<String> filesList = new List<String>();
        filesList.add('Test');
        filesList.add('Test');
        filesList.add('Test');
        filesList.add('Test');
        UploadClinicImagesController.uploadClinicImages(filesList,JSON.serialize(accountRecord));
    }
}