@isTest
public class PaymentMethodsControllerTest {
    @testSetup static void initMethod() {
        List<Stripe_Credentials__c> credentials = new List<Stripe_Credentials__c>();
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        credentials.add(stripeCredentail);
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        credentials.add(stripeCredentail1);
        
        Stripe_Credentials__c stripeCredentail2 = New Stripe_Credentials__c();
        stripeCredentail2.Name = 'Create Payment Method';
        stripeCredentail2.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail2.EndPoint__c = 'www.testEndpoint.com';
        credentials.add(stripeCredentail2);
        
        Stripe_Credentials__c stripeCredentail3 = New Stripe_Credentials__c();
        stripeCredentail3.Name = 'Attach Payment Method';
        stripeCredentail3.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        stripeCredentail3.EndPoint__c = 'www.testEndpoint.com{methodId}';
        credentials.add(stripeCredentail3);
        insert credentials;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Account';
        accountRecord.Stripe_Customer_Account__c = 'cusj00192';
        accountRecord.Email__c = 'test@testmail.com';
        accountRecord.Phone = '981046502';
        accountRecord.BillingStreet = 'TEST';
        accountRecord.BillingState = 'TEST';
        accountRecord.BillingCountry = 'TEST';
        accountRecord.BillingPostaLCode = '12345';
        accountRecord.BillingCity = 'TEST';
        insert accountRecord;
    }
    @isTest static void createPaymentMethodTest(){
        Account accountRecord = [SELECT Id,Name,Email__c,Phone,BillingStreet,BillingCity,BillingState,BillingCountry,BillingPostaLCode,Stripe_Customer_Account__c FROM Account LIMIT 1];
        String cardString = '{"cardNumber":"4111111111111111","cardExpMonth":"04","cardExpYear":"2024","cardCVC":"123"}';
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        
        PaymentMethodsController.createPaymentMethod(cardString,accountRecord);
        PaymentMethodsController.getAllPaymentMethods(accountRecord.Stripe_Customer_Account__c);
        PaymentMethodsController.getCustomer(accountRecord.Stripe_Customer_Account__c);
        PaymentMethodsController.deleteCreditCard('pytm007123');
        PaymentMethodsController.setDefaultCreditCard('pytm007123',accountRecord.Stripe_Customer_Account__c);
        PaymentMethodsController.getAccount(accountRecord.Id);
    }
}