public class PatientLoginController {
    @AuraEnabled
    public static List<Patient__c> checkLoginAuth(String username, String password) {

        List<Patient__c> patientUser = new List<Patient__c>();
        patientUser = [SELECT Id, Username__c, Password__c FROM Patient__c WHERE Username__c=:username AND Password__c=:password LIMIT 1];

        return patientUser;
    }
}