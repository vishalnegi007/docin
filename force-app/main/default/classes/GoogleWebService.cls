public with sharing class GoogleWebService {
    
    public static Http http = new Http();
    public static HTTPResponse response;
    public static HttpRequest request;
    
    public static String createFolder(String folderName){
        request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('callout:DocINAPICred/drive/v3/files');
        request.setHeader('content-type', 'application/json');  
        String body = '{"name" : "'+folderName+'","mimeType" : "application/vnd.google-apps.folder"}'; 
        request.setBody(body);  
        Httpresponse resp = http.send(request); 
        if(resp.getStatusCode() <= 299) {
            System.debug( resp.getBody()+'===='+resp.getStatus()  );
            Map<String,object> responseMap =(Map<String,object>)JSON.deserializeUntyped(resp.getBody());  
            system.debug('========='+responseMap);
            return responseMap.get('id').toString();
        }
        else{
            System.debug( resp.getBody()+'===='+resp.getStatus());
            return '';
        }
    }
    
    public static String uploadFileInFolder(String imageData,String imageName,String fileId,String folderId){
        String boundary = 'SalesforceNewsTechnologyStuff';
        String delimiter = '\r\n--' + boundary + '\r\n';  
        String close_delim = '\r\n--' + boundary + '--';  
        
        String bodyEncoded = imageData;  
        
        // String body = delimiter + 'Content-Type: application/json\r\n\r\n' + '{ "title" : "' + imageName + '",' + ' "mimeType" : "image/jpeg", "parents": [{"id": "'+folderId+'"}]}' + delimiter + 'Content-Type: image/jpeg' + '\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + bodyEncoded + close_delim;  
        String body = delimiter + 'Content-Type: application/json\r\n\r\n' + '{ "title" : "' + imageName + '",' + ' "mimeType" : "image/jpeg",' + '"parents":[{"id":"'+ folderId +'"}]}' + delimiter + 'Content-Type: image/jpeg\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + bodyEncoded + close_delim;
        
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type', 'multipart/related; boundary='+boundary);
        req.setHeader('Content-length', String.valueOf(body.length()));
        if(fileId!=null){
            req.setMethod('PUT'); 
            req.setEndpoint('callout:DocINAPICred/upload/drive/v2/files/'+fileId); 
        }else{
            req.setMethod('POST'); 
            req.setEndpoint('callout:DocINAPICred/upload/drive/v2/files?uploadType=multipart'); 
        }
        
        req.setBody(body); 
        Http h = new Http(); 
        try {
            Httpresponse resp = h.send(req);
            if(resp.getStatusCode() <= 299) {
                System.debug( resp.getBody()+'===='+resp.getStatus()  );
                Map<String,object> responseMap =(Map<String,object>)JSON.deserializeUntyped(resp.getBody());  
                system.debug('========='+responseMap);
                changeFilePermissions(responseMap.get('id').toString());
                return responseMap.get('id').toString();
            }
            else{
                System.debug( resp.getBody()+'===='+resp.getStatus());
                return '';
            }
        }
        catch(Exception ex) {
            System.debug('===='+ex.getCause() +' '+ ex.getLineNumber() +' '+ ex.getMessage());   
        }
        return '';
    }

    public static Boolean changeFilePermissions(String fileId){
        request = new HttpRequest();
        request.setMethod('POST');
        request.setEndpoint('callout:DocINAPICred/drive/v2/files/'+fileId+'/permissions');
        request.setHeader('content-type', 'application/json');  
        String body = '{"role":"reader","type":"anyone"}';
        request.setBody(body);  
        Httpresponse resp = http.send(request); 
        if(resp.getStatusCode() <= 299) {
            System.debug( resp.getBody()+'===='+resp.getStatus()  );
            Map<String,object> responseMap =(Map<String,object>)JSON.deserializeUntyped(resp.getBody());  
            system.debug('========='+responseMap);
            return true;
        }
        else{
            System.debug( resp.getBody()+'===='+resp.getStatus());
            return false;
        }
    } 
    public static String getFile(String fileId) {
        
        request = new HttpRequest();
        request.setMethod('GET');
        request.setEndpoint('callout:DocINAPICred/drive/v3/files/'+fileId+'?fields=*');
        request.setHeader('fields', '*');
        response = http.send(request); 
        
        System.debug(response.getBody());
        Map<String,object> responseMap =(Map<String,object>)JSON.deserializeUntyped(response.getBody()) ;  
        System.debug('webContentLink---- >  '+responseMap.get('webContentLink'));
        String thumbnailLink = String.valueOf(responseMap.get('webContentLink'));
        return thumbnailLink;

    }
    public static String uploadFile(String imageData,String userId,String fileId) {
        String boundary = 'SalesforceNewsTechnologyStuff';
        String delimiter = '\r\n--' + boundary + '\r\n';  
        String close_delim = '\r\n--' + boundary + '--';  
        
        String bodyEncoded = imageData;  
        
        String body = delimiter + 'Content-Type: application/json\r\n\r\n' + '{ "name" : "' + userId + '",' + ' "mimeType" : "image/jpeg"}' + delimiter + 'Content-Type: image/jpeg' + '\r\n' + 'Content-Transfer-Encoding: base64\r\n' + '\r\n' + bodyEncoded + close_delim;  
        
        HttpRequest req = new HttpRequest();
        req.setheader('Content-Length',String.valueOf(body.length()));
        req.setheader('Content-Type','multipart/related; boundary='+boundary);
        
        if(fileId!=null){
            req.setMethod('PUT'); 
            req.setEndpoint('callout:DocINAPICred/upload/drive/v2/files/'+fileId); 
        }else{
            req.setMethod('POST'); 
            req.setEndpoint('callout:DocINAPICred/upload/drive/v3/files?uploadType=multipart'); 
        }
        
        req.setBody(body); 
        Http h = new Http(); 
        try {
            Httpresponse resp = h.send(req);
            if(resp.getStatusCode() <= 299) {
                System.debug( resp.getBody()+'===='+resp.getStatus()  );
                Map<String,object> responseMap =(Map<String,object>)JSON.deserializeUntyped(resp.getBody());  
                system.debug('========='+responseMap);
                changeFilePermissions(responseMap.get('id').toString());
                return responseMap.get('id').toString();
            }
            else{
                System.debug( resp.getBody()+'===='+resp.getStatus());
                return '';
            }
        }
        catch(Exception ex) {
            System.debug('===='+ex.getCause() +' '+ ex.getLineNumber() +' '+ ex.getMessage());   
        }
        return '';
    }
}