public without sharing class TestAndProdceduresController {
    @AuraEnabled
    public static List<Test_Procedures__c> getProcedureRecords(String recordId){
        try {
            List<Test_Procedures__c> testProceduresList = new List<Test_Procedures__c>();
            testProceduresList = [SELECT Id, Existing_Patient_Fees__c, New_Patient_Fees__c, Procedure_Name__c, Procedure_Test__c, Service_Provider__c
                                  FROM Test_Procedures__c
                                  WHERE Service_Provider__c=:recordId
                                  LIMIT 10000];
            return testProceduresList;
        } catch (Exception e) {
            return null;
        }
    }
    @AuraEnabled
    public static Boolean saveAllProcedures(String procedureString){
        try {
            List<Test_Procedures__c> testAndProcedures = (List<Test_Procedures__c>) JSON.deserialize(procedureString, List<Test_Procedures__c>.class);
            upsert testAndProcedures;
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    @AuraEnabled
    public static Boolean deleteProcedure(String procedureId){
        try {
            delete [SELECT Id FROM Test_Procedures__c WHERE Id=:procedureId LIMIT 1];
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    @AuraEnabled
    public static List<String> getProceduresAndTestList(){
        try {
            List<String> proceduresList = new List<String>();
            Schema.DescribeFieldResult fieldResult = Test_Procedures__c.Procedure_Name__c.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                proceduresList.add(pickListVal.getLabel());
            }
            return proceduresList;
        } catch (Exception e) {
            return null;
        }
    }
}