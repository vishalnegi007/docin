@isTest
public class GetReviewsWebserviceTest {
	@isTest
    public static void GetReviewsTest(){
        
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
		);
        insert u;
        String requestJson = '{"userId":"'+u.Id+'"}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getReviews';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	GetReviewsWebservice.getAllReviews();
        test.stopTest();
        List<Rating__c> lstrating = [select id, name from Rating__c];
        System.assertEquals(lstrating.size(),0);
    }
    
    @isTest
    public static void GetReviewsTest12(){
        
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
		);
        insert u;
        String requestJson = '{"userId":}';
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/getReviews';
        req.requestBody = Blob.valueOf(requestJson);
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        
        test.startTest();
        	GetReviewsWebservice.getAllReviews();
        test.stopTest();
        List<Rating__c> lstrating = [select id, name from Rating__c];
        System.assertEquals(lstrating.size(),0);
    }
}