@isTest
public class UpdateWorkingHoursControllerTest {
    @isTest static void getUpdatedHoursTest(){
        Stripe_Credentials__c stripeCredentail = New Stripe_Credentials__c();
        stripeCredentail.Name = 'Create Customer';
        stripeCredentail.Endpoint__c = 'https://api.stripe.com/v1/customers';
        insert stripeCredentail;
        
        Stripe_Credentials__c stripeCredentail1 = New Stripe_Credentials__c();
        stripeCredentail1.Name = 'Secret Key';
        stripeCredentail1.Secret_Key__c = 'sk_test_51J5EE5SGd3pr63A55Dmlw1L3neYs1g38AK3OWbywiSMjhniBenCl3nImjRvbzlTxfAZUOQ4QXiTolwsarQ8xC2QC00lOWdX72J';
        insert stripeCredentail1;
        
        Test.setMock(HttpCalloutMock.class, new HttpMockCallout());
        Account accountRecord = new Account();
        accountRecord.Name = 'Test Account';
        accountRecord.Closed_Days__c = 'Saturday;Sunday;Tuesday;Wednesday;Thursday;Friday';
        accountRecord.Monday_Starting_Hours__c = Time.newInstance(16,30,00,000);
        insert accountRecord;
        
        UpdateWorkingHoursController.getUpdatedHours(accountRecord.Id);
        UpdateWorkingHoursController.updateAccount(JSON.serialize(accountRecord));
    }
}