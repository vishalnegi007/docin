public without sharing class ResetPassword_DOCIN_Controller {

    @AuraEnabled
    public static Boolean resetUserPassword(String userId, String password){
        List<Patient__c> user = new List<Patient__c>();
        user = [SELECT Id,Password__c FROM Patient__c WHERE Id =:userId LIMIT 1];
        System.debug('user---> '+user);
        if(!user.isEmpty()){
            user[0].Password__c = password;
            update user;
            return true;
        }else{
            return false;
        }
    }

    @AuraEnabled
    public static Boolean sendForgotPasswordEmail(String username){
        try {
            List<Patient__c> user = new List<Patient__c>();
            user = [SELECT Id,Name,Email__c,Last_Name__c,Username__c FROM Patient__c WHERE Username__c =:username OR Email__c=:username LIMIT 1];
            if(!user.isEmpty()){
                Messaging.SingleEmailMessage userEmail = new Messaging.SingleEmailMessage();
                userEmail.setSubject('Welcome to DOC-IN : Reset Password Request');
                String userEmailBody = '';
                userEmailBody += 'Hi '+user[0].Name +' '+user[0].Last_Name__c+',<br></br>';
                userEmailBody += 'We have received a request to reset your password.<br></br><br></br>';
                userEmailBody += 'To reset your password click <a href="'+System.Label.DOCIN_Base_URL+'ResetPassword_DOCIN?userId='+user[0].Id+'">here</a> <br></br>';

                userEmail.setHtmlBody(userEmailBody);
                userEmail.setToAddresses(new String[] {user[0].Email__c});
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{userEmail},false);
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            return false;        
        }
    }
}