public class CreatePatients {
    
    @AuraEnabled
    public static String insertPatientData(String patientRecord){
        System.debug('patientRecord::'+patientRecord);
        Patient__c patientRecordInstance = New Patient__c();
        patientRecordInstance = (Patient__c)JSON.deserialize(patientRecord, Patient__c.class);
        
        List<Patient__c> userList = new List<Patient__c>();
        userList = [SELECT Id,Name,Username__c,Email__c FROM Patient__c 
                    WHERE Email__c=:patientRecordInstance.Email__c OR Username__c =:patientRecordInstance.Username__c
                    LIMIT 1];
        
        if(userList.isEmpty()) {
            insert patientRecordInstance;
            sendEmailToPatients(patientRecordInstance);
            return 'SUCCESS';
        }
        else{
            return 'Username/Email exist in salesforce.';
        }
        
    }

@AuraEnabled
public static Boolean sendEmailToPatients(Patient__c patientObj){
    try {
        //Sending Email to patient
        //EmailTemplate patientTemplate = [SELECT Id,Subject,HtmlValue,DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Appointment_Confirmation_for_Patients' LIMIT 1];
        Messaging.SingleEmailMessage patientEmail = new Messaging.SingleEmailMessage();
        patientEmail.setSubject('Welcome to DOC-IN : Signup Successfully');
        String patientEmailBody = '';
        patientEmailBody += 'Hi '+patientObj.Name +' '+patientObj.Last_Name__c+',<br></br>';
        patientEmailBody += 'Welcome to DOC-IN. Your Account has been created successfully.<br></br><br></br>';
        patientEmailBody += 'Username : '+patientObj.Username__c+'<br></br>';

        patientEmail.setHtmlBody(patientEmailBody);
        patientEmail.setToAddresses(new String[] {patientObj.Email__c});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{patientEmail},false);
        return true;
    } catch (Exception e) {
        return false;
    }
}
}