public without sharing class FindADoctorController {
    
    @AuraEnabled
    public static String getAllDoctorsInfo(){
        AllClinicsInformation_Webservice.Response allClinincsWrapper = AllClinicsInformation_Webservice.getAllClinicsInformation();
        return JSON.serialize(allClinincsWrapper.data);
    }
    
    @AuraEnabled
    public static List<Patient__c> getUserInformation(String userId){
        Map <String, Schema.SObjectType> schemaMap1 = Schema.getGlobalDescribe();
        List<String> userFields = new List<String>();
        Map <String, Schema.SObjectField> fieldMap1 = schemaMap1.get('Patient__c').getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : fieldMap1.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            userFields.add(dField.getName());
        } 
        String userQuery = '';
        userQuery += 'SELECT '+ String.join(userFields, ',') +' FROM Patient__c WHERE Id = :userId';
        
        return database.query(userQuery);
    }
    
    @AuraEnabled
    public static List<List<SObject>> getSpecialtiesAndTests(String accountId){
        try {
            List<Test_Procedures__c> testsAndProcedures = new List<Test_Procedures__c>();
            List<Service_Provider__c> services = new List<Service_Provider__c>();
            services = [SELECT Id,Name,Specialty__c,Contact__c,Existing_Patient_Fees__c,New_Patient_Fees__c,Service_Provider__c,Visit_Type__c,Visit_Type_Name__c
                                 FROM Service_Provider__c 
                                 WHERE Service_Provider__c = :accountId
                                 LIMIT 10000];
            testsAndProcedures = [SELECT Id,Name,Existing_Patient_Fees__c,New_Patient_Fees__c,Procedure_Test__c,Procedure_Name__c,Service_Provider__c
                                 FROM Test_Procedures__c
                                 WHERE Service_Provider__c = :accountId
                                 LIMIT 10000];
            List<List<SObject>> servicesAndTestsList = new List<List<SObject>>();
            servicesAndTestsList.add(testsAndProcedures);
            servicesAndTestsList.add(services);
            return servicesAndTestsList;
        } catch (Exception e) {
            return null ;  
        }
    }

    @AuraEnabled
    public static String getDoctorInfo(String doctorId){
        Contact doctor = [SELECT Id,Doctor_Photo__c,FirstName,LastName,Ratings__c,Qualification__c,Languages_Spoken__c,Title,Email,Phone,Account.Name,AccountId,Account.BillingAddress,Summary__c,Specialty__c,
                          Account.Monday_Starting_Hours__c,Account.Monday_Ending_Hours__c,Account.Tuesday_Starting_Hours__c,Account.Tuesday_Ending_Hours__c,Account.Wednesday_Starting_Hours__c,Account.Wednesday_Ending_Hours__c,Account.Thursday_Starting_Hours__c,Account.Thursday_Ending_Hours__c,
                          Account.Friday_Starting_Hours__c,Account.Friday_Ending_Hours__c,Account.Saturday_Starting_Hours__c,Account.Saturday_Ending_Hours__c,Account.Sunday_Starting_Hours__c,Account.Sunday_Ending_Hours__c, 
                          (SELECT Id,Specialty__c,Contact__c,Description__c FROM Service_Provider__r WHERE Contact__c = :doctorId),
                          (SELECT Id,Name,Description__c,Doctor__c,Ratings__c,User__c,User__r.Name,User__r.Last_Name__c FROM Ratings__r WHERE Doctor__c = :doctorId)
                          FROM Contact
                          WHERE Id=:doctorId
                          LIMIT 1];
        return JSON.serialize(doctor);
    }
    public static void newFindADoctor(String clinic, String specialty, String doctor, String zipcode){
        List<Account> allClinics = new List<Account>();
        List<Contact> doctorsList = new List<Contact>();
        List<Service_Provider__c> specialityAndServices = new List<Service_Provider__c>();
        Map<String,Account> clinicsMap = new Map<String,Account>();
        
    }
    @AuraEnabled
    public static List<ClinicsWrapper> searchClinicsInformation(String clinicName, String specialty, String doctorName, String zipcode){
        List<ClinicsWrapper> clinicsInformation = new List<ClinicsWrapper>();
        try{
            List<Contact> doctorsList = new List<Contact>();
            List<Service_Provider__c> specialityAndServices = new List<Service_Provider__c>();
            List<Test_Procedures__c> testAndProcedures =  new List<Test_Procedures__c>();
            Map<String,List<Service_Provider__c>> specialitiesMap = new Map<String,List<Service_Provider__c>>();
            Map<String,List<Test_Procedures__c>> testAndProceduresMap = new Map<String,List<Test_Procedures__c>>();
            
            
            List<Account> allClinics = new List<Account>();
            //Querying CLinics
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            List<String> accountFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                accountFields.add(dField.getName());
            }
            //String clinicName;
            //String zipcode;
            String accountQuery = '';
            
            accountQuery += 'SELECT '+ String.join(accountFields, ',') +' FROM Account ';
            
            if(String.isNotBlank(clinicName) && String.isNotBlank(zipcode)){
                clinicName = '%'+clinicName+'%';
                zipcode = '%'+zipcode+'%';
                accountQuery += 'WHERE Name LIKE :clinicName AND BillingPostalCode LIKE :zipcode';            
            }
            if(String.isNotBlank(clinicName) && String.isBlank(zipcode)){
                clinicName = '%'+clinicName+'%';
                accountQuery += ' WHERE Name LIKE :clinicName'; 
            }
            if(String.isBlank(clinicName) && String.isNotBlank(zipcode)){
                zipcode = '%'+zipcode+'%';
                accountQuery += 'WHERE BillingPostalCode LIKE :zipcode';
            }
            
            Map<String,Account> clinicsMap = new Map<String,Account>(allClinics);
            Map <String, Schema.SObjectType> schemaMap1 = Schema.getGlobalDescribe();
            List<String> contactFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap1 = schemaMap1.get('Contact').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap1.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                contactFields.add(dField.getName());
            } 
            
            String contactQuery = '';
            contactQuery += 'SELECT '+ String.join(contactFields, ',') +',Account.BillingAddress FROM Contact';

            if(String.isNotBlank(specialty) && String.isNotBlank(doctorName)){
                specialty = '%'+specialty+'%';
                doctorName = '%'+doctorName+'%';
                contactQuery += ' WHERE Specialty__c LIKE :specialty AND (FirstName LIKE :doctorName OR LastName LIKE :doctorName OR Name LIKE :doctorName)';
            }

            if(String.isNotBlank(specialty) && String.isBlank(doctorName)){
                specialty = '%'+specialty+'%';
                contactQuery += ' WHERE Specialty__c LIKE :specialty';            
            }
            if(String.isBlank(specialty) && String.isNotBlank(doctorName)){
                doctorName = '%'+doctorName+'%';
                contactQuery += ' WHERE FirstName LIKE :doctorName OR LastName LIKE :doctorName OR Name LIKE :doctorName';            
            }

            if(String.isBlank(specialty) && String.isBlank(doctorName)){
                accountQuery += ' LIMIT 50000';
                allClinics = Database.query(accountQuery);
                contactQuery += ' WHERE AccountId IN :allClinics LIMIT 50000';
                doctorsList = Database.query(contactQuery);
            }else{
                contactQuery += ' LIMIT 50000';
                doctorsList = Database.query(contactQuery);
                Set<String> clinicIds = new Set<String>();
                for(Contact con : doctorsList){
                    if(con.AccountId!=null)
                        clinicIds.add(con.AccountId);
                }
                if(accountQuery.contains('WHERE')){
                    accountQuery += ' AND Id IN :clinicIds';
                }else{
                    accountQuery += ' WHERE Id IN :clinicIds';
                }
                accountQuery += ' LIMIT 50000';
                allClinics = Database.query(accountQuery);
            }
            clinicsMap.putAll(allClinics);

            Map <String, Schema.SObjectType> schemaMap11 = Schema.getGlobalDescribe();
            List<String> serviceProviderFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap11 = schemaMap11.get('Service_Provider__c').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap11.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                serviceProviderFields.add(dField.getName());
            }
            String serviceProviderQuery = '';
            serviceProviderQuery += 'SELECT '+ String.join(serviceProviderFields, ',') +',Contact__r.AccountId,Service_Provider__r.Name FROM Service_Provider__c'; 
            serviceProviderQuery += ' WHERE Service_Provider__c IN :allClinics';            
            serviceProviderQuery += ' LIMIT 50000';
            
            String testAnProcedureQuery = '';
            testAnProcedureQuery += 'SELECT Id,Name,Existing_Patient_Fees__c,New_Patient_Fees__c,Procedure_Test__c,Service_Provider__c';
            testAnProcedureQuery += ' FROM Test_Procedures__c WHERE Service_Provider__c IN :allClinics';
            testAnProcedureQuery += ' LIMIT 50000';
            
            //Querying Doctors, Specialties and tests/procedures

            specialityAndServices = Database.query(serviceProviderQuery);
            testAndProcedures = Database.query(testAnProcedureQuery);
            
        
            Map<String,List<Contact>> clinicAndDoctorsMap = new Map<String,List<Contact>>();
            
            for(Contact doctor : doctorsList){
                if(!clinicAndDoctorsMap.ContainsKey(doctor.AccountId))
                    clinicAndDoctorsMap.put(doctor.AccountId,new List<Contact>());
                clinicAndDoctorsMap.get(doctor.AccountId).add(doctor);
            }
            
            for(Service_Provider__c service : specialityAndServices){
                if(!specialitiesMap.ContainsKey(service.Service_Provider__c))
                    specialitiesMap.put(service.Service_Provider__c, new List<Service_Provider__c>());
                specialitiesMap.get(service.Service_Provider__c).add(service);
            }

            for(Test_Procedures__c procedure : testAndProcedures){
                if(!testAndProceduresMap.ContainsKey(procedure.Service_Provider__c))
                    testAndProceduresMap.put(procedure.Service_Provider__c, new List<Test_Procedures__c>());
                testAndProceduresMap.get(procedure.Service_Provider__c).add(procedure);
            }
            
            for(Account clinic : clinicsMap.values()){
                ClinicsWrapper clinincWrap = new ClinicsWrapper();
                List<DoctorAndSpecialitiesList> doctorAndSpecialistList = new List<DoctorAndSpecialitiesList>();
                if(clinicAndDoctorsMap.ContainsKey(clinic.Id)){
                    clinincWrap.doctors = clinicAndDoctorsMap.get(clinic.Id);
                }else{
                    clinincWrap.doctors = new List<Contact>();
                }
                if(specialitiesMap.ContainsKey(clinic.Id)){
                    clinincWrap.specialitiesAndServices = specialitiesMap.get(clinic.Id);
                }else{
                    clinincWrap.specialitiesAndServices = new List<Service_Provider__c>();
                }
                if(testAndProceduresMap.ContainsKey(clinic.Id)){
                    clinincWrap.testsAndProcedures = testAndProceduresMap.get(clinic.Id);
                }else{
                    clinincWrap.testsAndProcedures = new List<Test_Procedures__c>();
                }
                
                List<String> clinicImg = new List<String>();
                if(clinic.Clinic_Image_1__c!=null){
                    clinicImg.add(clinic.Clinic_Image_1__c);
                }
                if(clinic.Clinic_Image_2__c!=null){
                    clinicImg.add(clinic.Clinic_Image_2__c);
                }
                if(clinic.Clinic_Image_3__c!=null){
                    clinicImg.add(clinic.Clinic_Image_3__c);
                }
                if(clinic.Clinic_Image_4__c!=null){
                    clinicImg.add(clinic.Clinic_Image_4__c);
                }
                if(clinic.Clinic_Image_5__c!=null){
                    clinicImg.add(clinic.Clinic_Image_5__c);
                }
                
                clinincWrap.clinic = clinic;
                clinincWrap.clinicImages = clinicImg;
                clinicsInformation.add(clinincWrap);
            }
        }catch(Exception ex){
            System.debug('Error in System');
        }
        System.debug('clinicsInformation---> '+clinicsInformation);
        return clinicsInformation;
    }
    
    @AuraEnabled
    public static Appointment__c createAppointmentRecord(String appointmentString){
        try{
            System.debug('appointmentString---> '+appointmentString);
            Appointment__c appointment = (Appointment__c)JSON.deserialize(appointmentString,Appointment__c.class);
            insert appointment;
            Contact doctor = [SELECT Id,Name,Email,FirstName,LastName,Account.Email__c,Account.Name FROM Contact WHERE Id = :appointment.Doctor__c LIMIT 1];
            
            //Sending Emails to Doctor and Patient
            sendAppointmentEmails(doctor,appointment);
            return appointment;
        }catch(Exception ex){
            System.debug('Error at line --> '+ex.getLineNumber() + 'due to---> '+ex.getMessage());
            return null;
        }
        
    }

    public static void sendAppointmentEmails(Contact doctor, Appointment__c appointment){
        List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
        String appointmentDetails ='';
        //Sending Email to Doctor
        if(doctor.Email!=null){
            EmailTemplate doctorTemplate = [SELECT Id,Subject,HtmlValue,DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Appointment_Confirmation_for_Doctors' LIMIT 1];
            Messaging.SingleEmailMessage doctorEmail = new Messaging.SingleEmailMessage();
            doctorEmail.setSubject(doctorTemplate.Subject);
            String emailBody = doctorTemplate.HtmlValue;
            
            if(emailBody.containsIgnoreCase('{!Contact.LastName}'))
                emailBody = emailBody.replace('{!Contact.LastName}', 'Dr. '+doctor.LastName);
            if(appointment.Appointment_Time__c!=null)
                emailBody = emailBody.replace('appointment', 'appointment on '+appointment.Appointment_Time__c);
            
            
            appointmentDetails += '<p>Patient Name : '+appointment.Patient_Name__c+'</p>';
            appointmentDetails += '<p>Patient Email : '+appointment.Patient_Email__c+'</p>';
            if(appointment.Appointment_Date__c!=null)
                appointmentDetails += '<p>Appointment Date : '+appointment.Appointment_Date__c.format()+'</p>';
            
            if(appointment.Appointment_Time__c!=null){
                Datetime dt = Datetime.newInstance(date.today(),appointment.Appointment_Time__c);
                List<String> timeDetails = dt.format('MMMMM dd, yyyy hh:mm:ss a').split(' ');
                appointmentDetails += '<p>Appointment Time : '+timeDetails[3].split(':')[0]+':'+timeDetails[3].split(':')[1]+' '+timeDetails[4]+'</p>';
            }
            
            emailBody = emailBody.replace('{!ApointmentDetails}', appointmentDetails);
            List<String> emailsList = new List<String>();
            //emailsList.add(doctor.Email);
            if(doctor.Account.Email__c!=null)
                emailsList.add(doctor.Account.Email__c);
                
            doctorEmail.setHtmlBody(emailBody);
            doctorEmail.setToAddresses(emailsList);
            emailMessages.add(doctorEmail); 
        }
        
        
        //Sending Email to patient
        EmailTemplate patientTemplate = [SELECT Id,Subject,HtmlValue,DeveloperName FROM EmailTemplate WHERE DeveloperName = 'Appointment_Confirmation_for_Patients' LIMIT 1];
        Messaging.SingleEmailMessage patientEmail = new Messaging.SingleEmailMessage();
        String sub = patientTemplate.Subject;
        if(sub.containsIgnoreCase('{ClinicName}'))
            sub = sub.replace('{ClinicName}',doctor.Account.Name);
        patientEmail.setSubject(sub);
        String patientEmailBody = patientTemplate.HtmlValue;
        
        if(patientEmailBody.containsIgnoreCase('{!Paitent.Name}'))
            patientEmailBody = patientEmailBody.replace('{!Paitent.Name}', appointment.Patient_Name__c);
        if(patientEmailBody.containsIgnoreCase('{Clinic Name}'))
            patientEmailBody = patientEmailBody.replace('{Clinic Name}', doctor.Account.Name);
        
        appointmentDetails ='';
        appointmentDetails += '<p>Doctor Name : '+doctor.FirstName +' '+doctor.LastName+'</p>';
        if(appointment.Specialty__c!=null)
            appointmentDetails += '<p>Specialty : '+appointment.Specialty__c+'</p>';
        if(appointment.Appointment_Date__c!=null){
            
        }
        appointmentDetails += '<p>Appointment Date : '+appointment.Appointment_Date__c.format()+'</p>';
        if(appointment.Appointment_Time__c!=null){
            Datetime dt = Datetime.newInstance(date.today(),appointment.Appointment_Time__c);
            List<String> timeDetails = dt.format('MMMMM dd, yyyy hh:mm:ss a').split(' ');
            appointmentDetails += '<p>Appointment Time : '+timeDetails[3].split(':')[0]+':'+timeDetails[3].split(':')[1]+' '+timeDetails[4]+'</p>';
        }
        
        
        patientEmailBody = patientEmailBody.replace('{!Appointment Details}', appointmentDetails);
        
        patientEmail.setHtmlBody(patientEmailBody);
        patientEmail.setToAddresses(new String[] {appointment.Patient_Email__c});
        emailMessages.add(patientEmail);
        
        Messaging.sendEmail(emailMessages,false);
    }

    @AuraEnabled
    public static List<Rating__c> getRatings(String userId) {
        List<Rating__c> ratingsList = new List<Rating__c>();
        ratingsList = [SELECT Id,User__c,Doctor__c,Doctor__r.FirstName,Doctor__r.LastName,Ratings__c,Description__c
                        FROM Rating__c
                        WHERE User__c =:userId
                        LIMIT 50000];
        return ratingsList;
    }
    @AuraEnabled
    public static List<Appointment__c> getAllAppointments(String userId) {
        List<Appointment__c> appointmentsList = new List<Appointment__c>();
        appointmentsList = [SELECT Id,Name,Specialty__c,Doctor__r.Phone,Doctor__r.Account.Name,Patient_Name__c,Patient_Email__c,Appointment_Date__c,Appointment_Time__c,Patient_Phone__c, Patient__c, Patient__r.Name, Patient__r.Last_Name__c, Doctor__c,Doctor__r.Title, Doctor__r.FirstName,Doctor__r.LastName
                            FROM Appointment__c 
                            WHERE Patient__c = :userId
                            ORDER BY Appointment_Date__c DESC
                            LIMIT 50000];

        return appointmentsList;
    }
    
    @AuraEnabled
    public static Patient__c updateUserProfile(Patient__c userRecord) {
        update userRecord;
        return userRecord;
    }

    @AuraEnabled
    public static Patient__c uploadUserProfilePhoto(String imageData,String userId) {
        String imageId ;
        String thumbnailLink;
        List<Patient__c> userProfiles = new List<Patient__c>();
        userProfiles = getUserInformation(userId);//[SELECT Id,LastName,Doctor_Photo__c,Doctor_Photo_ID__c FROM Contact WHERE Id =:userId LIMIT 1];

        imageId = GoogleWebService.uploadFile(imageData,'user '+userId,userProfiles[0].Profile_Photo_ID__c);
        thumbnailLink = GoogleWebService.getFile(imageId);
       
        System.debug('thumbnailLink--> '+thumbnailLink);
        Patient__c userProfile = userProfiles[0];
        userProfile.Id = userId;
        userProfile.User_Image__c	 = thumbnailLink;
        userProfile.Profile_Photo_ID__c = imageId;
        update userProfile;
        return userProfile;
    }

    @AuraEnabled
    public static FilterWrapper getSearchFilters(){
        FilterWrapper filterwrapper = new FilterWrapper();
        List<Account> clinicNames = new List<Account>();
        List<String> specialty = new List<String>();
        clinicNames = [SELECT Id,Name FROM Account ORDER BY Name LIMIT 50000 ];
        
        Schema.DescribeFieldResult fieldResult = Service_Provider__c.Specialty__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            specialty.add(pickListVal.getLabel());
        }
        filterwrapper.clinics = clinicNames;
        filterwrapper.specialities = specialty;
        return filterwrapper;
    }
    public class ClinicsWrapper{
        @AuraEnabled public Account clinic;
        @AuraEnabled public List<Contact> doctors;
        @AuraEnabled public List<String> clinicImages;
        @AuraEnabled public List<Service_Provider__c> specialitiesAndServices;
        @AuraEnabled public List<Test_Procedures__c> testsAndProcedures;
    }
    public class DoctorAndSpecialitiesList{
        @AuraEnabled public Contact doctor;
        @AuraEnabled public List<Service_Provider__c> specialitiesAndServices;
    }
    public class FilterWrapper{
        @AuraEnabled public List<Account> clinics;
        @AuraEnabled public List<String> specialities;
    }
}