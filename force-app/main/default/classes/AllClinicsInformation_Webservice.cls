@RestResource(urlMapping='/getAllClinics/*')
global with sharing class AllClinicsInformation_Webservice{
    // Post Method
    @HTTPPost
    global static Response getAllClinicsInformation(){
        List<ClinicsWrapper> clinicsInformation = new List<ClinicsWrapper>();
        Response response = new Response();
        try{
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            Map<String, Object> params ;
            if(req!=null && req.requestBody!=null && String.valueOf(req.requestBody)!='')
                params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            
            List<Contact> doctorsList = new List<Contact>();
            List<Service_Provider__c> specialityAndServices = new List<Service_Provider__c>();
            List<Test_Procedures__c> testAndProcedures =  new List<Test_Procedures__c>();
            Map<String,List<Service_Provider__c>> specialitiesMap = new Map<String,List<Service_Provider__c>>();
            Map<String,List<Test_Procedures__c>> testAndProceduresMap = new Map<String,List<Test_Procedures__c>>();
            
            
            List<Account> allClinics = new List<Account>();
            //Querying CLinics
            Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
            List<String> accountFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap = schemaMap.get('Account').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                accountFields.add(dField.getName());
            }
            String clinicName;
            String zipcode;
            String accountQuery = '';
            if(params!=null && params.containsKey('clinicName') && params.get('clinicName')!='' && params.get('clinicName')!=null){
                clinicName = String.valueOf(params.get('clinicName'));
            }
            if(params!=null && params.containsKey('zipCode') && params.get('zipCode')!='' && params.get('zipCode')!=null){
                zipcode = String.valueOf(params.get('zipCode'));
            }
            
            accountQuery += 'SELECT '+ String.join(accountFields, ',') +' FROM Account ';
            
            if(String.isNotBlank(clinicName) && String.isNotBlank(zipcode)){
                clinicName = '%'+clinicName+'%';
                zipcode = '%'+zipcode+'%';
                accountQuery += 'WHERE Name LIKE :clinicName AND BillingPostalCode LIKE :zipcode';            
            }
            if(String.isNotBlank(clinicName) && String.isBlank(zipcode)){
                clinicName = '%'+clinicName+'%';
                accountQuery += ' WHERE Name LIKE :clinicName'; 
            }
            if(String.isBlank(clinicName) && String.isNotBlank(zipcode)){
                zipcode = '%'+zipcode+'%';
                accountQuery += 'WHERE BillingPostalCode LIKE :zipcode';
            }
            
            Map<String,Account> clinicsMap = new Map<String,Account>(allClinics);
            Map <String, Schema.SObjectType> schemaMap1 = Schema.getGlobalDescribe();
            List<String> contactFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap1 = schemaMap1.get('Contact').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap1.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                contactFields.add(dField.getName());
            } 
                        
            String specialty;
            if(params!=null && params.containsKey('specialty') && params.get('specialty')!='' && params.get('specialty')!=null){
                specialty = String.valueOf(params.get('specialty'));
            }
            String doctorName;
            if(params!=null && params.containsKey('doctorName') && params.get('doctorName')!='' && params.get('doctorName')!=null){
                doctorName = String.valueOf(params.get('doctorName'));
            }
            String contactQuery = '';
            contactQuery += 'SELECT '+ String.join(contactFields, ',') +',Account.BillingAddress FROM Contact';

            if(String.isNotBlank(specialty) && String.isNotBlank(doctorName)){
                specialty = '%'+specialty+'%';
                doctorName = '%'+doctorName+'%';
                contactQuery += ' WHERE Specialty__c LIKE :specialty AND (FirstName LIKE :doctorName OR LastName LIKE :doctorName OR Name LIKE :doctorName)';
            }

            if(String.isNotBlank(specialty) && String.isBlank(doctorName)){
                specialty = '%'+specialty+'%';
                contactQuery += ' WHERE Specialty__c LIKE :specialty';            
            }
            if(String.isBlank(specialty) && String.isNotBlank(doctorName)){
                doctorName = '%'+doctorName+'%';
                contactQuery += ' WHERE FirstName LIKE :doctorName OR LastName LIKE :doctorName OR Name LIKE :doctorName';            
            }

            if(String.isBlank(specialty) && String.isBlank(doctorName)){
                accountQuery += ' LIMIT 50000';
                allClinics = Database.query(accountQuery);
                contactQuery += ' WHERE AccountId IN :allClinics LIMIT 50000';
                doctorsList = Database.query(contactQuery);
            }else{
                contactQuery += ' LIMIT 50000';
                doctorsList = Database.query(contactQuery);
                Set<String> clinicIds = new Set<String>();
                for(Contact con : doctorsList){
                    if(con.AccountId!=null)
                        clinicIds.add(con.AccountId);
                }
                if(accountQuery.contains('WHERE')){
                    accountQuery += ' AND Id IN :clinicIds';
                }else{
                    accountQuery += ' WHERE Id IN :clinicIds';
                }
                accountQuery += ' LIMIT 50000';
                allClinics = Database.query(accountQuery);
            }
            clinicsMap.putAll(allClinics);

            Map <String, Schema.SObjectType> schemaMap11 = Schema.getGlobalDescribe();
            List<String> serviceProviderFields = new List<String>();
            Map <String, Schema.SObjectField> fieldMap11 = schemaMap11.get('Service_Provider__c').getDescribe().fields.getMap();
            for(Schema.SObjectField sfield : fieldMap11.Values()){
                schema.describefieldresult dfield = sfield.getDescribe();
                serviceProviderFields.add(dField.getName());
            }
            String serviceProviderQuery = '';
            serviceProviderQuery += 'SELECT '+ String.join(serviceProviderFields, ',') +',Contact__r.AccountId,Service_Provider__r.Name FROM Service_Provider__c'; 
            serviceProviderQuery += ' WHERE Service_Provider__c IN :allClinics';            
            serviceProviderQuery += ' LIMIT 50000';
            
            String testAnProcedureQuery = '';
            testAnProcedureQuery += 'SELECT Id,Name,Existing_Patient_Fees__c,New_Patient_Fees__c,Procedure_Test__c,Service_Provider__c';
            testAnProcedureQuery += ' FROM Test_Procedures__c WHERE Service_Provider__c IN :allClinics';
            testAnProcedureQuery += ' LIMIT 50000';
            
            //Querying Doctors, Specialties and tests/procedures

            specialityAndServices = Database.query(serviceProviderQuery);
            testAndProcedures = Database.query(testAnProcedureQuery);
            
           
            Map<String,List<Contact>> clinicAndDoctorsMap = new Map<String,List<Contact>>();
            
            for(Contact doctor : doctorsList){
                if(!clinicAndDoctorsMap.ContainsKey(doctor.AccountId))
                    clinicAndDoctorsMap.put(doctor.AccountId,new List<Contact>());
                clinicAndDoctorsMap.get(doctor.AccountId).add(doctor);
            }
            
            for(Service_Provider__c service : specialityAndServices){
                if(!specialitiesMap.ContainsKey(service.Service_Provider__c))
                    specialitiesMap.put(service.Service_Provider__c, new List<Service_Provider__c>());
                specialitiesMap.get(service.Service_Provider__c).add(service);
            }

            for(Test_Procedures__c procedure : testAndProcedures){
                if(!testAndProceduresMap.ContainsKey(procedure.Service_Provider__c))
                    testAndProceduresMap.put(procedure.Service_Provider__c, new List<Test_Procedures__c>());
                testAndProceduresMap.get(procedure.Service_Provider__c).add(procedure);
            }
            
            for(Account clinic : clinicsMap.values()){
                ClinicsWrapper clinincWrap = new ClinicsWrapper();
                List<DoctorAndSpecialitiesList> doctorAndSpecialistList = new List<DoctorAndSpecialitiesList>();
                if(clinicAndDoctorsMap.ContainsKey(clinic.Id)){
                    clinincWrap.doctors = clinicAndDoctorsMap.get(clinic.Id);
                }else{
                    clinincWrap.doctors = new List<Contact>();
                }
                if(specialitiesMap.ContainsKey(clinic.Id)){
                    clinincWrap.specialitiesAndServices = specialitiesMap.get(clinic.Id);
                }else{
                    clinincWrap.specialitiesAndServices = new List<Service_Provider__c>();
                }
                if(testAndProceduresMap.ContainsKey(clinic.Id)){
                    clinincWrap.testsAndProcedures = testAndProceduresMap.get(clinic.Id);
                }else{
                    clinincWrap.testsAndProcedures = new List<Test_Procedures__c>();
                }
                
                List<String> clinicImg = new List<String>();
                if(clinic.Clinic_Image_1__c!=null){
                    clinicImg.add(clinic.Clinic_Image_1__c);
                }
                if(clinic.Clinic_Image_2__c!=null){
                    clinicImg.add(clinic.Clinic_Image_2__c);
                }
                if(clinic.Clinic_Image_3__c!=null){
                    clinicImg.add(clinic.Clinic_Image_3__c);
                }
                if(clinic.Clinic_Image_4__c!=null){
                    clinicImg.add(clinic.Clinic_Image_4__c);
                }
                if(clinic.Clinic_Image_5__c!=null){
                    clinicImg.add(clinic.Clinic_Image_5__c);
                }
                
                clinincWrap.clinic = clinic;
                clinincWrap.clinicImages = clinicImg;
                clinicsInformation.add(clinincWrap);
            }
            
            response.status_code = '200';
            response.message = 'Clinics Fetched Successfully';
            response.data = clinicsInformation;
        }catch(Exception ex){
            response.status_code = '400';
            response.message = 'Error in fetching Clinics'+ex.getMessage()+ex.getLineNumber();
            response.data = clinicsInformation;
        }
        
        
        return response;
    }
    
    global class ClinicsWrapper{
        global Account clinic;
        global List<Contact> doctors;
        global List<String> clinicImages;
        global List<Service_Provider__c> specialitiesAndServices;
        global List<Test_Procedures__c> testsAndProcedures;
    }
    global class DoctorAndSpecialitiesList{
        global Contact doctor;
        global List<Service_Provider__c> specialitiesAndServices;
    }
    global class Response{
        public String message;
        public String status_code;
        public List<ClinicsWrapper> data;
    }
}