@RestResource(urlMapping='/resetPassword/*')
global class ResetPassword_Wwebservice {
    @HTTPPost
    global static Response resetUserPassword() {
        Response response = new Response();
        try{
            RestRequest req = RestContext.request;
            Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(req.requestBody.toString());
            String userEmail = params.get('email').toString();
            List<Patient__c> patientRecord = new List<Patient__c>();
            patientRecord = [SELECT Id,Email__c,Name,Last_Name__c,Gender__c,Password__c,Phone__c,Username__c
                             FROM Patient__c
                             WHERE Email__c = :userEmail
                             LIMIT 1];
            if(!patientRecord.isEmpty()){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setSubject('Reset Your Password');
                message.setHtmlBody('Reset your password by clicking here');
                message.setToAddresses(new String[] { patientRecord[0].Email__c });
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});
                response.status_code = '200';
                response.message = 'Email sent successfully to '+patientRecord[0].Email__c;
            }else{
                response.status_code = '200';
                response.message = 'User not Found';
            }
        }catch(Exception ex){
            response.status_code = '400';
            response.message = 'Error in fetching profile';
        }
        return response;
    }
    
    global class Response{
        global String message;
        global String status_code;
    }
}