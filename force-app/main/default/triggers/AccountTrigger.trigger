trigger AccountTrigger on Account (before insert,after insert) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            AccountTriggerHandler.createStripeAccount(Trigger.new);
        }
    }
}