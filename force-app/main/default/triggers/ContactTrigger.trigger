trigger ContactTrigger on Contact (before insert,after insert) {

    if(Trigger.isAfter && Trigger.isInsert){
        ContactTriggerHandler.createDefaultImage(Trigger.new);
    }
}