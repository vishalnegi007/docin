({
    handleSignup: function (component, event, helper) {
        var action = component.get("c.registerUser");
        action.setParams({
            userInfo:JSON.stringify(component.get("v.SignupInfo"))
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                component.set("v.RegistrationResponse",response.getReturnValue());
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})