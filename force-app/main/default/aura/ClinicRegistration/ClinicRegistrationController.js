({
    handleInit: function (component, event, helper) {
        var signupInfo = {'practiceName':'',
        'firstName':'','lastName':'','userName':'','email':'','password':'','confirmPassword':''};
        component.set("v.SignupInfo",signupInfo);
    },
    handleSignup: function (component, event, helper) {
        var allValid = component.find('info').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        if (allValid) {
            alert('All form entries look valid. Ready to submit!');
        } else {
            alert('Please update the invalid form entries and try again.');
            return;
        }
        var signupInfo = component.get("v.SignupInfo");
        console.log("signupInfo--> "+JSON.stringify(signupInfo));
        helper.handleSignup(component, event, helper);
    }

})