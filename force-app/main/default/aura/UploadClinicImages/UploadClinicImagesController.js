({
    handleInit: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.getAccount(component, event, helper);
    },
    handleFilesChange: function (component, event, helper) {
        helper.uploadHelper(component, event, helper);
    },
    uploadFiles: function (component, event, helper) {
        var filesListToUpload = component.get("v.filesListToUpload");
        var mandatoryCheck = false;
        for (var i in filesListToUpload) {
            if (filesListToUpload[i] == '' || filesListToUpload[i] == undefined || filesListToUpload[i] == null)
                mandatoryCheck = true;
        }
        if (mandatoryCheck) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Error",
                "type":"error",
                "message": "Please Enter Image in all the rows"
            });
            toastEvent.fire();
            return;
        }
        helper.showSpinner(component, event, helper);
        helper.uploadFiles(component, event, helper);
    },
    showUpload: function (component, event, helper) {
        component.set("v.showUploader", false);
    },
    hideUpload: function (component, event, helper) {
        component.set("v.showUploader", true);
        component.set("v.filesList", []);
    },
    addRow: function (component, event, helper) {
        var filesList = component.get("v.filesList");
        if (filesList.length < 5) {
            filesList.push('');
            component.set("v.filesList", filesList);
            component.set("v.filesListToUpload", filesList);
        }
    },
    deleteRow: function (component, event, helper) {
        var whichOne = event.getSource().get("v.name");
        var filesList = component.get("v.filesList");
        filesList.splice(whichOne, 1)
        component.set("v.filesList", filesList);
        component.set("v.filesListToUpload", filesList);
    },
})