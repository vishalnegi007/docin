({
    getAccount: function (component, event, helper) {
        var action = component.get("c.getClinicImages");
        action.setParams({
            accountId: component.get("v.recordId")
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {
                console.log('-response.getReturnValue()--> ' + JSON.stringify(result));
                component.set("v.AccountRecord", result.accountRecord);
                component.set("v.ImageWrapper", result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
        var filesList = component.get("v.filesList");
        filesList.push('');
        component.set("v.filesList", filesList);
    },
    uploadFiles: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.uploadClinicImages");
        console.log('component.get("v.filesListToUpload")---> ' + JSON.stringify(component.get("v.filesListToUpload")));
        console.log('component.get("v.AccountRecord")--> ' + JSON.stringify(component.get("v.AccountRecord")));
        action.setParams({
            filesList: component.get("v.filesListToUpload"),
            accountRecord: JSON.stringify(component.get("v.AccountRecord"))
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            var result = response.getReturnValue();
            if (state == 'SUCCESS') {
                console.log('-response.getReturnValue()--> ' + JSON.stringify(result));
                if (result = true) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!",
                        "type": "success",
                        "message": "Clinic Images Uploaded Successfully"
                    });
                    toastEvent.fire();
                    helper.getAccount(component, event, helper);
                    component.set("v.showUploader", true);
                    component.set("v.filesList", []);
                    component.set("v.filesListToUpload", []);
                   
                } else {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error",
                        "type": "error",
                        "message": "Error in uploading clinic Images"
                    });
                    toastEvent.fire();
                }
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);

    },
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 

    uploadHelper: function (component, event, helper) {
        // start/show the loading spinner   

        // get the selected files using aura:id [return array of files]
        var fileInput = event.getSource().get("v.files");

        var file = fileInput[0];
        console.log("file--> " + file.type);
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {
            alert("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
        }

        // create a FileReader object 
        var objFileReader = new FileReader();
        // set onload function of FileReader object   
        objFileReader.onload = $A.getCallback(function () {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;

            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component, event, helper, file, fileContents);
        });

        objFileReader.readAsDataURL(file);

        // get the first file using array index[0]  

    },

    uploadProcess: function (component, event, helper, file, fileContents) {
        // set a default size or startpostiton as 0 
        var startPosition = 0;
        // calculate the end size or endPostion using Math.min() function which is return the min. value   
        var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
        var index = event.getSource().get("v.name");
        var filesListToUpload = component.get("v.filesListToUpload");
        var filesList = component.get("v.filesList");
        filesListToUpload[index] = fileContents;
        fileContents = 'data:' + file.type + ';base64,' + fileContents;
        filesList[index] = fileContents;
        component.set("v.filesList", filesList);
        component.set("v.filesListToUpload", filesListToUpload);
        // start with the initial chunk, and set the attachId(last parameter)is null in begin
        console.log('filesList--> ' + filesList);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})