({
    closeModal : function(component, event, helper) {
        component.set("v.showLoginConfirmation",false);
    },
    
    openSignup : function(component, event, helper) {
        window.location.href = $A.get("$Label.c.DOCIN_Base_URL")+'RegistrationForm';
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"RegistrationForm","_self");
    },
    openLogin : function(component, event, helper) {
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"Login","_self");
    },
})