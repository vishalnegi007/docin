({
    handleForgotPassword : function(component, event, helper) {
        var Username = component.get("v.Username");
        var action = component.get("c.sendForgotPasswordEmail");
        action.setParams({
            "username" : Username
        });
        action.setCallback(this, function(response){
        var result = response.getReturnValue();
            console.log("result--> "+result);
            component.set("v.emailDeliveryStatus",result);
            component.set("v.loginError",result);
        });
        $A.enqueueAction(action);
    }
})