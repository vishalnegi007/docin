({
    handleInit : function(component, event, helper) {
        var action = component.get("c.getUserInformation");
        action.setParams({
            "userId": component.get("v.User")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("User Record--- " + JSON.stringify(result));
                component.set("v.UserRecord",result[0]);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    }, 
    getUserRatings: function(component, event, helper) {
        var action = component.get("c.getRatings");
        action.setParams({
            "userId": component.get("v.User")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result--> " + JSON.stringify(result));
                component.set("v.Ratings",result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    getAppointments: function(component, event, helper) {
        var action = component.get("c.getAllAppointments");
        action.setParams({
            "userId": component.get("v.User")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result--> " + JSON.stringify(result));
                component.set("v.Appointments",result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 
    
    uploadHelper: function(component, event,helper) {
        // get the selected files using aura:id [return array of files]
        var fileInput = component.find("fileId").get("v.files");
        // get the first file using array index[0]  
        var file = fileInput[0];
        var self = this;
        // check the selected file size, if select file size greter then MAX_FILE_SIZE,
        // then show a alert msg to user,hide the loading spinner and return from function  
        if (file.size > self.MAX_FILE_SIZE) {
            component.set("v.showLoadingSpinner", false);
            component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
            return;
        }
 
        // create a FileReader object 
        var objFileReader = new FileReader();
        // set onload function of FileReader object   
        objFileReader.onload = $A.getCallback(function() {
            var fileContents = objFileReader.result;
            var base64 = 'base64,';
            var dataStart = fileContents.indexOf(base64) + base64.length;
 
            fileContents = fileContents.substring(dataStart);
            // call the uploadProcess method 
            self.uploadProcess(component,event,helper, file, fileContents);
        });
 
        objFileReader.readAsDataURL(file);
    },
 
    uploadProcess: function(component,event,helper, file, fileContents) {
        console.log('fileContents--> '+fileContents);
        var action = component.get("c.uploadUserProfilePhoto");
        action.setParams({
            imageData: fileContents,
            userId : component.get("v.recordId")
        });
 
        // set call back 
        action.setCallback(this, function(response) {
            // store the response / Attachment Id   
            var result = response.getReturnValue();
            console.log('result--> '+JSON.stringify(result));
            var state = response.getState();
            if (state === "SUCCESS") {
                helper.hideSpinner(component, event, helper); 
                component.set("v.UserRecord",result);
                if(result.Doctor_Photo__c!=null)
                    component.set("v.showProfilePhoto",false);
                    location.reload();
                helper.hideSpinner(component, event, helper);       
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
                helper.hideSpinner(component, event, helper);
            } else if (state === "ERROR") {
                alert("From server: " + response.getReturnValue());
                helper.hideSpinner(component, event, helper);
             }
         });
        // enqueue the action
         $A.enqueueAction(action);
    },
    saveProfileData: function(component, event, helper) {
        var action = component.get("c.updateUserProfile");
        action.setParams({
            "userRecord": component.get("v.UserRecord")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result--> " + JSON.stringify(result));
                component.set("v.UserRecord",result);
                component.set("v.editForm", false);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
     // this function automatic call by aura:waiting event  
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})