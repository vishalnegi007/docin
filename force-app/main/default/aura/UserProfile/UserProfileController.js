({
    handleInit : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var myStorage = window.localStorage;
        console.log('UserName----> ' + localStorage.getItem('userId'));
        var userId = localStorage.getItem('userId');
        if (userId != null) {
            component.set("v.User", userId);
            component.set("v.recordId", userId);
            helper.handleInit(component, event, helper);
            helper.getUserRatings(component, event, helper);
            helper.getAppointments(component, event, helper);
        }
    },
    showUpload : function(component, event, helper) {
        component.set("v.showPhotoUploader", true);
    },
    hideUpload : function(component, event, helper) {
        component.set("v.showPhotoUploader", false);
    },
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
    doSave: function(component, event, helper) {
        if (component.find("fileId").get("v.files").length > 0) {
            helper.showSpinner(component, event, helper);
            helper.uploadHelper(component, event,helper);
        } else {
            alert('Please Select a Valid File');
        }
    },
    saveProfileData: function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.saveProfileData(component, event, helper);
    },
    makeFormEditable: function(component, event, helper) {
        component.set("v.editForm", true);
    },
    makeFormInEditable: function(component, event, helper) {
        component.set("v.editForm", false);
    }
})