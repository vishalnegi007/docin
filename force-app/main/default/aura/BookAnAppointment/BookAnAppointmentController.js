({
    handleInit : function(component, event, helper) {
        
        helper.getApointmentDetails(component,event,helper);
        helper.getFilterList(component,event,helper);
    },
    createApointment: function(component, event, helper){
        var myStorage = window.localStorage;
        console.log('UserName----> ' + localStorage.getItem('userId'));
        var userId = localStorage.getItem('userId');
        if(userId==null || userId==undefined){
            console.log("not Logger In");
            return;
        }
        helper.showSpinner(component, event, helper);
        helper.createApointment(component, event, helper);
    },
    openProfile : function(component, event, helper){
        helper.showSpinner(component, event, helper);
        helper.openProfile(component, event, helper);
    }
})