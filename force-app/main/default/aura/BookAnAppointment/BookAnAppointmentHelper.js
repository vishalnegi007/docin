({
    getApointmentDetails: function(component, event, helper) {
        var action = component.get("c.getDoctorInfo");
        action.setParams({
            doctorId:component.get("v.DoctorId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                var doctor = JSON.parse(response.getReturnValue());
                component.set("v.Doctor",doctor);
                
                helper.getTestsAndServices(component, event, helper, doctor.AccountId);
                
                var patient = component.get("v.UserRecord");
                var appointment = component.get("v.Appointment");
                appointment.Patient_Name__c = patient.Name + ' '+patient.Last_Name__c;
                appointment.Patient__c = patient.Id;
                appointment.Patient_Email__c = patient.Email__c;
                appointment.Patient_Phone__c = patient.Phone__c;
                appointment.Patient_Date_of_Birth__c = patient.Date_of_Birth__c;
                appointment.Clinic__c = doctor.AccountId;
                appointment.Specialty__c = doctor.Specialty__c;
                appointment.Doctor__c = doctor.Id;
                component.set("v.Appointment",appointment);
                console.log("v.appointment---> "+JSON.stringify(appointment));
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    getTestsAndServices: function (component, event, helper,account_Id) {
        var action = component.get("c.getSpecialtiesAndTests");
        action.setParams({
            accountId: account_Id
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result---> "+JSON.stringify(result));
                component.set("v.TestAndProcedures",result[0]);
                component.set("v.Services",result[1]);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    getFilterList : function(component, event, helper) {
        var action = component.get("c.getSearchFilters");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                component.set("v.FilterValues",response.getReturnValue());
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    createApointment: function(component, event, helper) {
        var allValid = component.find('app').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        
        if (allValid) {
            var appointment = component.get("v.Appointment");
            // var user = component.get("v.UserRecord");
            // if(user!=null && user.Id!=null){
            //     appointment.Patient_Name__c = user.Name +' '+user.Last_Name__c;
            //     appointment.Patient__c = user.Id;
            // }
            // appointment.Doctor__c = component.get("v.DoctorId");
            // appointment.Clinic__c = component.get("v.Doctor").AccountId;
            console.log('JSON.stringify(component.get("v.Appointment")----> '+JSON.stringify(component.get("v.Appointment")));
            var action = component.get("c.createAppointmentRecord");
            action.setParams({
                appointmentString : JSON.stringify(appointment)
            });
            action.setCallback(this,function(response){
                var state = response.getState();
                if(state=='SUCCESS'){
                    console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                    //component.set("v.FilterValues",response.getReturnValue());
                    component.set("v.AppointmentConfirmation",true);
                    helper.hideSpinner(component, event, helper);
                }else{
                    console.log('error');
                }
            })
            $A.enqueueAction(action);
        }else{
            helper.hideSpinner(component, event, helper);
        }
    },
    openProfile : function(component, event, helper) {
        var doctorId = component.get("v.DoctorId");
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"DoctorProfile?doctorId="+doctorId,"_self");
        console.log('doctorId--> '+doctorId);
        var evt = component.getEvent("ViewProfile");    
        try{
            evt.setParams({
                "DoctorId":doctorId
            });
            console.log('evt--> '+evt);
            evt.fire();
        }catch(err){
            console.log(err);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})