({
    openRegForm : function(component, event, helper) {
        window.location.href = $A.get("$Label.c.DOCIN_Base_URL")+'RegistrationForm';
    },
       
    handleLogin : function(component, event, helper) {
        component.set("v.loginError",false);
        helper.showSpinner(component, event, helper);
        helper.handleLogin(component, event, helper);
    },
})