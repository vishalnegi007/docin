({
    handleLogin: function (component, event, helper) {
        try {
            var userName = component.get("v.Username");
            var passWord = component.get("v.Password");
            var action = component.get("c.checkLoginAuth");
            action.setParams({
                "username": userName,
                "password": passWord
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log('state--> ' + state);
                if (state == 'SUCCESS') {
                    var result = response.getReturnValue();
                    console.log("result--> " + JSON.stringify(result));
                    if (result.length != 0) {

                        var myStorage = window.localStorage;
                        localStorage.setItem('userId', result[0].Id); 
                        console.log('asdasdasd--> ' + localStorage.getItem('userId'));
                        var baseUrl = $A.get("$Label.c.DOCIN_Base_URL")+"FindADoctor?";
                        baseUrl += "clinic=&";
                        baseUrl += "specialty=&";
                        baseUrl += "doctor=&";
                        baseUrl += "zipcode=";
                        window.open(baseUrl, "_self");
                    } else {
                        component.set("v.loginError", true);
                    }
                    helper.hideSpinner(component, event, helper);
                } else {
                    console.log('error');
                    helper.hideSpinner(component, event, helper);
                }
            })

            $A.enqueueAction(action);
        } catch (err) {
            console.log(err);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})