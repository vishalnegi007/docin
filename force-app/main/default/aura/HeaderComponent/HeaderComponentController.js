({
    handleInit: function (component, event, helper) {
        var myStorage = window.localStorage;
        console.log('UserName----> ' + localStorage.getItem('userId'));
        var userId = localStorage.getItem('userId');
        if (userId != null) {
            component.set("v.User", userId);
            helper.handleInit(component, event, helper);
        }

    },
    openLogin: function (component, event, helper) {
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"Login", "_self");
    },
    handleSelect: function (component, event, helper) {
        var actionName = event.getParam("value");
        console.log("actonNAm---> " + actionName);
        if (actionName == 'Logout') {
            localStorage.clear();
            window.open($A.get("$Label.c.DOCIN_Base_URL")+"Login", "_self");
        } else if (actionName == 'Profile') {
            window.open($A.get("$Label.c.DOCIN_Base_URL")+"UserProfile", "_self");
        }
    }
})