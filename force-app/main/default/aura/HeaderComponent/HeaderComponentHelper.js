({
    handleInit : function(component, event, helper) {
        var action = component.get("c.getUserInformation");
            action.setParams({
                "userId": component.get("v.User")
            });

            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log('state--> ' + state);
                if (state == 'SUCCESS') {
                    var result = response.getReturnValue();
                    console.log("result--> " + JSON.stringify(result));
                    component.set("v.UserRecord",result[0]);
                    helper.hideSpinner(component, event, helper);
                } else {
                    console.log('error');
                    helper.hideSpinner(component, event, helper);
                }
            })

            $A.enqueueAction(action);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})