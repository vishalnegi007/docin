({
    handleInit: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.getAccount");
        action.setParams({
            "recordId": component.get("v.recordId")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.result--> " + JSON.stringify(result));
                component.set("v.AccountRecord", result);
                helper.getPaymentMethods(component, event, helper);
                helper.getStripeCustomer(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    getStripeCustomer: function (component, event, helper) {
        var action = component.get("c.getCustomer");
        var account = component.get("v.AccountRecord");
        action.setParams({
            "customerId": account.Stripe_Customer_Account__c
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.Stripe Customer--> " + JSON.stringify(result));
                if(result.invoice_settings.default_payment_method != undefined && result.invoice_settings.default_payment_method != null){
                    component.set("v.DefaultPaymentMethod",result.invoice_settings.default_payment_method);
                }
                component.set("v.StripCustomer", result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    getPaymentMethods: function (component, event, helper) {
        var action = component.get("c.getAllPaymentMethods");
        var account = component.get("v.AccountRecord");
        action.setParams({
            "customerId": account.Stripe_Customer_Account__c
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.result--> " + JSON.stringify(result));
                component.set("v.PaymentMethods", result.data);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    deleteCard: function (component, event, helper) {
        var action = component.get("c.deleteCreditCard");
        var paymentId = event.getSource().get("v.name");
        action.setParams({
            "paymentMethodId":paymentId
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.result--> " + JSON.stringify(result));
                helper.getPaymentMethods(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    makeDefault: function (component, event, helper) {
        var action = component.get("c.setDefaultCreditCard");
        var account = component.get("v.AccountRecord");
        var paymentId = event.getSource().get("v.name");
        action.setParams({
            "paymentMethodId":paymentId,
            "customerId": account.Stripe_Customer_Account__c
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.result--> " + JSON.stringify(result));
                if(result.invoice_settings.default_payment_method != undefined && result.invoice_settings.default_payment_method != null){
                    component.set("v.DefaultPaymentMethod",result.invoice_settings.default_payment_method);
                }
                component.set("v.StripCustomer", result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    addCard: function (component, event, helper) {
        console.log("carObject---> " + JSON.stringify(component.get("v.CardObject")));
        console.log("AccountRecord---> " + JSON.stringify(component.get("v.AccountRecord")));
        var action = component.get("c.createPaymentMethod");
        action.setParams({
            "cardObjectString": JSON.stringify(component.get("v.CardObject")),
            "accountRecord": component.get("v.AccountRecord")
        });

        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log('-response.getReturnValue()--> ' + JSON.stringify(result));
                if (result.error != null || result.error != undefined) {
                    component.set("v.errorMessage", result.error.message);
                } else {
                    component.set("v.showAddCardPopup", false);
                    helper.handleInit(component, event, helper);
                }
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})