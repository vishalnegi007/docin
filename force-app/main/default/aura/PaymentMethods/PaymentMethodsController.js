({
    handleInit: function (component, event, helper) {
        var startYear = new Date().getFullYear();
        var years = [];
        for (var i = startYear; i <= startYear + 50; i++) {
            years.push(i);
        }
        console.log('years--> ' + years);
        component.set("v.ExpYears", years);
        var cardObject = {
            cardNumber: '', cardExpMonth: '', cardExpYear: '', cardCVC: ''
        };
        component.set("v.CardObject", cardObject);
        helper.showSpinner(component, event, helper);
        helper.handleInit(component, event, helper);
    },
    makeDefault: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.makeDefault(component, event, helper);
    },
    deleteCard: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.deleteCard(component, event, helper);
    },
    addCardPopup: function (component, event, helper) {
        component.set("v.showAddCardPopup", true);
    },

    closeCardPopup: function (component, event, helper) {
        component.set("v.showAddCardPopup", false);
    },
    addCard: function (component, event, helper) {
        var allValid = component.find('cardValue').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        if (allValid) {
            helper.showSpinner(component, event, helper);
            helper.addCard(component, event, helper);
        }
    },
})