({
    getFilterList : function(component, event, helper) {
        var action = component.get("c.getSearchFilters");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                component.set("v.FilterValues",response.getReturnValue());
                //helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                //helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
})