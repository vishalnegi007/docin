({
    handleInit: function (component, event, helper) {
        if(screen.width<=480){
            component.set("v.showMobileNavBar",true);
        }
        helper.getFilterList(component, event, helper);
    },
    openDoctorLogin: function (component, event, helper) {
        window.open($A.get("$Label.c.Doctor_Portal_URL")+"s/login", "_self");
    },
    handleSelect: function (component, event, helper) {
        var personType = event.getParam("value");
        console.log("--> " + personType);
        if (personType == 'Doctor') {
            window.open($A.get("$Label.c.Doctor_Portal_URL")+"s/login", "_self");
        }else if(personType == 'List your Practice'){
            window.open($A.get("$Label.c.Doctor_Portal_URL")+'CommunitiesSelfReg?startURL=/cash2careclinics','_self');
        } else {
            window.open($A.get("$Label.c.DOCIN_Base_URL")+"Login", "_self");
        }

    },
    searchClinics: function (component, event, helper) {
        var clinic_1 = component.get("v.clinic");
        var specialty_1 = component.get("v.specialty");
        var doctor_1 = component.get("v.doctor");
        var zipcode_1 = component.get("v.zipcode");
        var baseUrl = $A.get("$Label.c.DOCIN_Base_URL")+"FindADoctor?";
        baseUrl += "clinic=" + clinic_1 + "&";
        baseUrl += "specialty=" + specialty_1 + "&";
        baseUrl += "doctor=" + doctor_1 + "&";
        baseUrl += "zipcode=" + zipcode_1;
        window.open(baseUrl, "_self");
    }

})