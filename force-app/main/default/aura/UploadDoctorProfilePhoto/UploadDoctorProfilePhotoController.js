({
    handleInit : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.handleInit(component, event, helper);
    },
    removePhoto: function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.removePhoto(component, event, helper);
    },
    showUpload: function(component, event, helper) {
        component.set("v.showProfilePhoto",false);
    },
    hideUpload: function(component, event, helper) {
        component.set("v.showProfilePhoto",true);
    },
    doSave: function(component, event, helper) {
        if (component.find("fileId").get("v.files").length > 0) {
            helper.showSpinner(component, event, helper);
            helper.uploadHelper(component, event,helper);
        } else {
            alert('Please Select a Valid File');
        }
    },
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },
})