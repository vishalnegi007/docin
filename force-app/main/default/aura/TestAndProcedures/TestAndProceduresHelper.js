({
    handleInit: function (component, event, helper) {
        var action = component.get("c.getProcedureRecords");
		action.setParams({
			recordId : component.get("v.recordId")
		});
		action.setCallback(this,function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state=='SUCCESS'){
                console.log('result--> '+JSON.stringify(result));
                try{
                    for(let element of result){
                        console.log('element.Procedure_Name__c:::'+element.Procedure_Name__c);
                        if(element.Procedure_Name__c === 'Other') {
                            component.set("v.count", parseInt(component.get("v.count") + 1));
                            element.displayProcedureTestField = true;
                        }
                        else {
                            element.displayProcedureTestField = false;
                        }
                    }
                    if(component.get("v.count") > 0){
                        component.set("v.displayProcedureColumn", true);
                    }
                    console.log('result211--> '+JSON.stringify(result));
                    component.set("v.TestAndProceduresList",result);
                }catch(err){
                    console.log(err);
                }
				helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    getProcedureList: function (component, event, helper) {
        var action = component.get("c.getProceduresAndTestList");
		action.setCallback(this,function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state=='SUCCESS'){
                console.log('result--> '+JSON.stringify(result));
                component.set("v.procedureList",result);
				helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    validate:  function (component, event, helper) {
        let validate = true;
        if(component.find("testProcedure").length) {
            for(let i = 0; i < component.find("testProcedure").length; i++){
                let inputElement = component.find("testProcedure")[i];
                if(!inputElement.get("v.value")) {
                    inputElement.setCustomValidity("Complete this field.");
                    inputElement.reportValidity();
                    validate = false;
                }
                else {
                    inputElement.setCustomValidity("");
                    inputElement.reportValidity();
                }
            }
        }
        else if(component.find("testProcedure")) {
            let inputElement = component.find("testProcedure");
            if(!inputElement.get("v.value")) {
                inputElement.setCustomValidity("Complete this field.");
                inputElement.reportValidity();
                validate = false;
            }
            else {
                inputElement.setCustomValidity("");
                inputElement.reportValidity();
            }
        }
        
        if(component.find("procedurePicklist").length) {
            for(let i = 0; i < component.find("procedurePicklist").length; i++){
                let inputElement = component.find("procedurePicklist")[i];
                if(!inputElement.get("v.value")) {
                    inputElement.showHelpMessageIfInvalid();
                    validate = false;
                }
                else {
                    inputElement.showHelpMessageIfInvalid();
                }
            }
        }
        else if(component.find("procedurePicklist")) {
            let inputElement = component.find("procedurePicklist");
            if(!inputElement.get("v.value")) {
                inputElement.showHelpMessageIfInvalid();
                validate = false;
            }
            else {
                inputElement.showHelpMessageIfInvalid();
            }
        }
        
        
        if(validate) {
            component.set("v.validate", true);
        }
        else {
            component.set("v.validate", false);
        }
    },
    saveProcedures: function (component, event, helper) {
        console.log(JSON.stringify(component.get("v.TestAndProceduresList")));
        
        let testAndProceduresList = component.get("v.TestAndProceduresList");
        for(let element of testAndProceduresList){
            if(element.Procedure_Name__c !== 'Other') {
                element.Procedure_Test__c = element.Procedure_Name__c;
            }
            delete element.displayProcedureTestField;
        }
        //console.log(JSON.stringify(component.get("v.TestAndProceduresList")));
        var action = component.get("c.saveAllProcedures");
        action.setParams({
			procedureString : JSON.stringify(testAndProceduresList)
		});
		action.setCallback(this,function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state=='SUCCESS'){
                console.log('result--> '+JSON.stringify(result));
                component.set("v.count", 0);
                helper.handleInit(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    deleteRow: function (component, event, helper) {
        var procedure_Id = event.getSource().get("v.title");
        var action = component.get("c.deleteProcedure");
        action.setParams({
			procedureId : procedure_Id
		});
		action.setCallback(this,function(response){
            var state = response.getState();
            var result = response.getReturnValue();
            if(state=='SUCCESS'){
                console.log('result--> '+JSON.stringify(result));
                helper.handleInit(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
     // this function automatic call by aura:waiting event  
     showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
    handleProcedurePicklist : function(component, event, helper){
        let index = event.getSource().get("v.name");
        console.log('index::'+index);
        console.log('count::'+component.get("v.count"));
        let testAndProceduresList = component.get("v.TestAndProceduresList");
        
        if(component.find("procedurePicklist").length && 
           component.find("procedurePicklist")[index].get("v.value") === 'Other') {
            if(testAndProceduresList[index].Procedure_Name__c === 'Other') {
                component.set("v.count", parseInt(component.get("v.count") + 1));
            }
            testAndProceduresList[index].displayProcedureTestField = true;
        }
        else if(component.find("procedurePicklist").get("v.value") === 'Other'){
            if(testAndProceduresList[index].Procedure_Name__c === 'Other') {
                component.set("v.count", parseInt(component.get("v.count") + 1));
            }
            testAndProceduresList[index].displayProcedureTestField = true;
        }
        else {
            console.log('else');
            if(testAndProceduresList[index].displayProcedureTestField === true) {
                component.set("v.count", parseInt(component.get("v.count") - 1));
            }
            testAndProceduresList[index].displayProcedureTestField = false;
        }
        component.set("v.TestAndProceduresList", testAndProceduresList);
        
        if(component.get("v.count") > 0){
            component.set("v.displayProcedureColumn", true);
        }
        else {
            component.set("v.displayProcedureColumn", false);
        }
    }
})