({
    handleInit: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.handleInit(component, event, helper);
        helper.getProcedureList(component, event, helper);
    },
    addRow: function (component, event, helper) {
        var proceduresList = component.get("v.TestAndProceduresList");
        var newProcedure = {
            "Service_Provider__c":component.get("v.recordId"),
            "Procedure_Test__c":'',
            "Existing_Patient_Fees__c":0,
            "New_Patient_Fees__c":0
        };
        proceduresList.push(newProcedure);
        component.set("v.TestAndProceduresList",proceduresList);
    },
    saveProcedures: function (component, event, helper) {
        if(component.get("v.displayProcedureColumn")) {
            helper.validate(component, event, helper);
        }
        if(component.get("v.validate")) {
            helper.showSpinner(component, event, helper);
            helper.saveProcedures(component, event, helper);
        }
    },
    deleteRow: function (component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.deleteRow(component, event, helper);
    },
    handleProcedurePicklist : function (component, event, helper) {
        helper.handleProcedurePicklist(component, event, helper);
    }
})