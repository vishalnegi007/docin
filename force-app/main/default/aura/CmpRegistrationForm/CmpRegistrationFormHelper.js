({
    validate: function (component, event, helper) {
        let validate = true;

        let firstname = component.find("firstname");
        if (!firstname.get("v.value")) {
            firstname.setCustomValidity("Complete this field.");
            firstname.reportValidity();
            validate = false;
        } else {
            firstname.setCustomValidity("");
            firstname.reportValidity();
        }

        let lastname = component.find("lastname");
        if (!lastname.get("v.value")) {
            lastname.setCustomValidity("Complete this field.");
            lastname.reportValidity();
            validate = false;
        } else {
            lastname.setCustomValidity("");
            lastname.reportValidity();
        }

        let email = component.find("email");
        if (!email.get("v.value")) {
            email.setCustomValidity("Complete this field.");
            email.reportValidity();
            validate = false;
        } else {
            email.setCustomValidity("");
            email.reportValidity();
        }

        let dateOfBirth = component.find("dateOfBirth");
        if (!dateOfBirth.get("v.value")) {
            dateOfBirth.setCustomValidity("Complete this field.");
            dateOfBirth.reportValidity();
            validate = false;
        } else {
            dateOfBirth.setCustomValidity("");
            dateOfBirth.reportValidity();
        }

        let username = component.find("username");
        if (!username.get("v.value")) {
            username.setCustomValidity("Complete this field.");
            username.reportValidity();
            validate = false;
        } else {
            username.setCustomValidity("");
            username.reportValidity();
        }

        let phone = component.find("phone");
        if (!phone.get("v.value")) {
            phone.setCustomValidity("Complete this field.");
            phone.reportValidity();
            validate = false;
        } else {
            phone.setCustomValidity("");
            phone.reportValidity();
        }

        let password = component.find("password");
        if (!password.get("v.value")) {
            password.setCustomValidity("Complete this field.");
            password.reportValidity();
            validate = false;
        } else {
            password.setCustomValidity("");
            password.reportValidity();
        }

        let rePassword = component.find("rePassword");
        if (!rePassword.get("v.value")) {
            rePassword.setCustomValidity("Complete this field.");
            rePassword.reportValidity();
            validate = false;
        } else {
            rePassword.setCustomValidity("");
            rePassword.reportValidity();
        }

        let errorList = [];

        if (password.get("v.value") && rePassword.get("v.value") &&
            password.get("v.value") != rePassword.get("v.value")) {
            errorList.push('password did not match');
        }

        component.set("v.errorList", errorList);

        component.set("v.validate", validate);
        helper.hideSpinner(component, event, helper);
        helper.submitForm(component, event, helper);
    },

    submitForm: function (component, event, helper) {
        console.log('component.get("v.validate")---> ' + component.get("v.validate"));
        if (component.get("v.validate")) {
            helper.showSpinner(component, event, helper);
            var patientRecord = component.get("v.patientRecord");
            console.log("patientRecord:::" + JSON.stringify(patientRecord));
            var action = component.get("c.insertPatientData");

            action.setParams({
                "patientRecord": JSON.stringify(patientRecord)
            });

            action.setCallback(this, function (response) {
                var state = response.getState();

                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    console.log("result--> " + result);
                    if (result === 'SUCCESS') {
                        component.set("v.showSignupConfirmation",true);
                        helper.hideSpinner(component, event, helper);
                    } else {
                        let errorList = [];
                        errorList.push(result);
                        component.set("v.errorList", errorList);
                        helper.hideSpinner(component, event, helper);
                    }
                    //var intervalInMilliseconds = 300; 
                    //component.set("v.showSuccess",false);
                    //window.location.href = $A.get("$Label.c.DOCIN_Base_URL")+'Login';
                }
            });
            $A.enqueueAction(action);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})