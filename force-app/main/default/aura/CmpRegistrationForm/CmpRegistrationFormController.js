({
    openRegForm : function(component, event, helper) {
        component.set("v.showRegistration",true);
    },
    submitForm : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var reEnterPassword = component.get("v.reEnterPassword");
        
        helper.validate(component, event, helper);
        
    },
    
    backForm : function(component, event, helper) {
         window.location.href = $A.get("$Label.c.DOCIN_Base_URL")+'Login';
    }
})