({
    handleInit : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.getProfileInfo(component, event, helper);
    },
    bookAppointment : function(component, event, helper) {
        var myStorage = window.localStorage;
        console.log('UserName----> ' + localStorage.getItem('userId'));
        var userId = localStorage.getItem('userId');
        if(userId==null || userId==undefined){
            component.set("v.showLoginConfirmation",true);
            console.log("not Logger In");
            return;
        }
        var doctorId = component.get("v.DoctorId");
        console.log('doctorId--> '+doctorId);
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"BookAnAppointment?doctorId="+doctorId,"_self");
        var evt = component.getEvent("BookAnAppointment");    
        try{
            evt.setParams({
                "DoctorId":doctorId
            });
            console.log('evt--> '+evt);
            evt.fire();
        }catch(err){
            console.log(err);
        }
    },
    showVisitTypeFees : function(component, event, helper) {
        var services = component.get("v.Services");
        var serviceId = event.getSource().get("v.value");
        for(var i in services){
            if(services[i].Id == serviceId){
                component.set("v.SelectedService",services[i]);
                break;
            }else{
                component.set("v.SelectedService",{});
            }
        }
    },
    showTestFees : function(component, event, helper) {
        var testId = event.getSource().get("v.value");
        var testAndProcedures = component.get("v.TestAndProcedures");
        for(var i in testAndProcedures){
            if(testAndProcedures[i].Id == testId){
                component.set("v.SelectedTest",testAndProcedures[i]);
                break;
            }else{
                component.set("v.SelectedTest",{});
            }
        }
    },
})