({
    getProfileInfo: function (component, event, helper) {
        var doctor_Id = component.get("v.DoctorId");
        var action = component.get("c.getDoctorInfo");
        action.setParams({
            doctorId: doctor_Id
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var result = JSON.parse(response.getReturnValue());
                helper.getTestsAndServices(component, event, helper,result.AccountId);
                console.log("result---> " + JSON.stringify(result));
                if(result.Ratings__r!=null && result.Ratings__r!=undefined)
                	component.set("v.Reviews",result.Ratings__r.records);
                component.set('v.mapMarkers', [
                    {
                        location: {
                            Street: result.Account.BillingAddress.street,
                            City: result.Account.BillingAddress.city,
                            State: result.Account.BillingAddress.state,
                            ZipCode:result.Account.BillingAddress.postalCode
                        },
        
                        title: result.Account.Name,
                        description: ''
                    }
                ]);
                try {
                    function formatAMPM(hours, minutes) {
                        var ampm = hours >= 12 ? 'PM' : 'AM';
                        hours = hours % 12;
                        hours = hours ? hours : 12; // the hour '0' should be '12'
                        var strTime = hours + ':' + minutes + ' ' + ampm;
                        return strTime;
                    }
                    var theTime;
                    if (result.Account.Monday_Starting_Hours__c != null) {
                        theTime = result.Account.Monday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Monday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Monday_Ending_Hours__c != null) {
                        theTime = result.Account.Monday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Monday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Tuesday_Starting_Hours__c != null) {
                        theTime = result.Account.Tuesday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Tuesday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Tuesday_Ending_Hours__c != null) {
                        theTime = result.Account.Tuesday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Tuesday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Wednesday_Starting_Hours__c != null) {
                        theTime = result.Account.Wednesday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Wednesday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Wednesday_Ending_Hours__c != null) {
                        theTime = result.Account.Wednesday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Wednesday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Thursday_Starting_Hours__c != null) {
                        theTime = result.Account.Thursday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Thursday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Thursday_Ending_Hours__c != null) {
                        theTime = result.Account.Thursday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Thursday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Friday_Starting_Hours__c != null) {
                        theTime = result.Account.Friday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Friday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Friday_Ending_Hours__c != null) {
                        theTime = result.Account.Friday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Friday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Saturday_Starting_Hours__c != null) {
                        theTime = result.Account.Saturday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Saturday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Saturday_Ending_Hours__c != null) {
                        theTime = result.Account.Saturday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Saturday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Sunday_Starting_Hours__c != null) {
                        theTime = result.Account.Sunday_Starting_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Sunday_Starting_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }
                    if (result.Account.Sunday_Ending_Hours__c != null) {
                        theTime = result.Account.Sunday_Ending_Hours__c.split('.')[0].substring(0, 5);
                        result.Account.Sunday_Ending_Hours__c = formatAMPM(theTime.split(':')[0], theTime.split(':')[1]);
                    }

                    
                } catch (Err) {
                    console.log(Err);
                }
                component.set("v.Doctor", result);

                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    getTestsAndServices: function (component, event, helper,account_Id) {
        var action = component.get("c.getSpecialtiesAndTests");
        action.setParams({
            accountId: account_Id
        })
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("result---> "+JSON.stringify(result[0]));
                console.log("result---> "+JSON.stringify(result[1]));
                component.set("v.TestAndProcedures",result[0]);
                component.set("v.Services",result[1]);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})