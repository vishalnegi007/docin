({
	doInit : function(component, event, helper) {
		var action = component.get("c.getUpdatedHours");
		action.setParams({
			recordId : component.get("v.recordId")
		});
		action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
				console.log('-response.getReturnValue()--> '+JSON.stringify(response.getReturnValue()));
                try{
                    var accRecord = JSON.parse(response.getReturnValue());
                var disableDays = {
                    "Monday":false,
                    "Tuesday":false,
                    "Wednesday":false,
                    "Thursday":false,
                    "Friday":false,
                    "Saturday":false,
                    "Sunday":false,
                };
                if(accRecord.Closed_Days__c!=null){
                    if(accRecord.Closed_Days__c.toLowerCase().includes('monday'))
                        disableDays.Monday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('tuesday'))
                        disableDays.Tuesday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('wednesday'))
                        disableDays.Wednesday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('thursday'))
                        disableDays.Thursday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('friday'))
                        disableDays.Friday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('saturday'))
                        disableDays.Saturday=true;
                    if(accRecord.Closed_Days__c.toLowerCase().includes('sunday'))
                        disableDays.Sunday=true;
                    console.log(accRecord.Closed_Days__c.split(';'));
                    component.set("v.DisabledDays",accRecord.Closed_Days__c.split(';'));
                }
                
                component.set("v.DisableRows",disableDays);
				component.set("v.AccountRecord",accRecord);
                }catch(err){
                    console.log(err);
                }
				helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
	},
	updateWorkingHours : function(component, event, helper){
        var disabledDays = component.get("v.DisabledDays"); 
		var account = component.get("v.AccountRecord");
		var action = component.get("c.updateAccount");
        if(disabledDays.length>0){
            var closedDays = disabledDays.join(';');
            account.Closed_Days__c = closedDays;
        }
        console.log("JSON.stringify(account)--. "+JSON.stringify(account));
		action.setParams({
			accountString : JSON.stringify(account)
		});
		action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
				console.log(response.getReturnValue());
				component.set("v.AccountRecord",JSON.parse(response.getReturnValue()));
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					title : 'Success!',
					message: 'Working Hours Saved Successfully',
					duration:' 2000',
					type:'success'
				});
				toastEvent.fire();
				helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
				var toastEvent = $A.get("e.force:showToast");
				toastEvent.setParams({
					title : 'Error!',
					message: 'Error in Saving Hours',
					duration:' 2000',
					type:'error'
				});
				toastEvent.fire();
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
	},
	 // this function automatic call by aura:waiting event  
	 showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})