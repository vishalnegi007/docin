({
	handleInit : function(component, event, helper) {
		helper.showSpinner(component, event, helper);
		helper.doInit(component, event, helper);
	},
	updateWorkingHours : function(component, event, helper){
		helper.showSpinner(component, event, helper);
		helper.updateWorkingHours(component, event, helper);
	},
    disableDay : function(component, event, helper){
        var accountRecord = component.get("v.AccountRecord");
        var day = event.getSource().get("v.name");
        var checked = event.getSource().get("v.checked");
        var disabledDays = component.get("v.DisabledDays");
        console.log('disabledDays--> '+JSON.stringify(disabledDays));
        console.log(day+"===="+checked);
        try{if(day=='monday' && checked){
            console.log('asdas');
            accountRecord.Monday_Starting_Hours__c=null;
            accountRecord.Monday_Ending_Hours__c=null;
            disabledDays.push('Monday');
        }else if(day=='monday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Monday'), 1);
        }
        if(day=='tuesday' && checked){
            accountRecord.Tuesday_Starting_Hours__c=null;
            accountRecord.Tuesday_Ending_Hours__c=null;
            disabledDays.push('Tuesday');
        }else if(day=='tuesday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Tuesday'), 1);
        }
        if(day=='wednesday' && checked){
            accountRecord.Wednesday_Starting_Hours__c=null;
            accountRecord.Wednesday_Ending_Hours__c=null;
            disabledDays.push('Wednesday');
        }else if(day=='wednesday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Wednesday'), 1);
        }
        if(day=='thursday' && checked){
            accountRecord.Thursday_Starting_Hours__c=null;
            accountRecord.Thursday_Ending_Hours__c=null;
            disabledDays.push('Thursday');
        }else if(day=='thursday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Thursday'), 1);
        }
        if(day=='friday' && checked){
            accountRecord.Friday_Starting_Hours__c=null;
            accountRecord.Friday_Ending_Hours__c=null;
            disabledDays.push('Friday');
        }else if(day=='friday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Friday'), 1);
        }
        if(day=='saturday' && checked){
            accountRecord.Saturday_Starting_Hours__c=null;
            accountRecord.Saturday_Ending_Hours__c=null;
            disabledDays.push('Saturday');
        }else if(day=='saturday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Saturday'), 1);
        }
        if(day=='sunday' && checked){
            accountRecord.Sunday_Starting_Hours__c=null;
            accountRecord.Sunday_Ending_Hours__c=null;
            disabledDays.push('Sunday');
        }else if(day=='sunday' && !checked){
            disabledDays.splice(disabledDays.indexOf('Sunday'), 1);
        }
           }catch(err){
               console.log(err);
           }
        console.log('disabledDays--> '+JSON.stringify(disabledDays));
        component.set("v.DisableDays",disabledDays);
        component.set("v.AccountRecord",accountRecord);
    }
})