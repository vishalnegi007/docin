({
    handleInit : function(component, event, helper) {
        var myStorage = window.localStorage;
        console.log('UserName----> '+localStorage.getItem('userId'));
        helper.showSpinner(component, event, helper);
        //helper.getAllDoctors(component, event, helper);
        helper.searchClinics(component, event, helper);
        helper.getFilterList(component,event,helper);
    },
    openProfile : function(component, event, helper) {
        var doctorId = event.target.name;
        console.log('doctorId--> '+doctorId);
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"DoctorProfile?doctorId="+doctorId,"_self");
        var evt = component.getEvent("ViewProfile");    
        try{
            evt.setParams({
                "DoctorId":doctorId
            });
            console.log('evt--> '+evt);
            evt.fire();
        }catch(err){
            console.log(err);
        }
    },
    bookAppointment : function(component, event, helper) {
        var myStorage = window.localStorage;
        console.log('UserName----> ' + localStorage.getItem('userId'));
        var userId = localStorage.getItem('userId');
        if(userId==null || userId==undefined){
            console.log("not Logger In");
            component.set("v.showLoginConfirmation",true);
            return;
        }
        helper.showSpinner(component, event, helper);
        var doctorId = event.target.dataset.id;
        window.open($A.get("$Label.c.DOCIN_Base_URL")+"BookAnAppointment?doctorId="+doctorId,"_self");
        console.log('doctorId--> '+doctorId);
        var evt = component.getEvent("BookAnAppointment");    
        try{
            evt.setParams({
                "DoctorId":doctorId
            });
            console.log('evt--> '+evt);
            evt.fire();
        }catch(err){
            console.log(err);
        }
    },
    searchClinics : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.searchClinics(component, event, helper);
    }
})