({
    getAllDoctors: function(component, event, helper) {
        var action = component.get("c.getAllDoctorsInfo");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                var result = response.getReturnValue();//JSON.parse(response.getReturnValue());
                console.log('-response.getReturnValue()--> '+JSON.stringify(result));
                console.log(JSON.stringify(result));
                component.set("v.AllClinicsWrapper",result);
                var doctorsList = [];
                for(var i in result){
                    for(var j in result[i].doctorsAndSpecialities){
                        doctorsList.push(result[i].doctorsAndSpecialities[j]);
                    }
                }
                console.log('doctorsList--> '+JSON.stringify(doctorsList));
                component.set("v.DoctorsAndSpecialtiesList",doctorsList);
                if(doctorsList.length>0){
                    component.set("v.NoDoctorFound",false);
                }else{
                    component.set("v.NoDoctorFound",true);
                }
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    getFilterList : function(component, event, helper) {
        var action = component.get("c.getSearchFilters");
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                console.log('Filter Values--> '+JSON.stringify(response.getReturnValue()));
                component.set("v.FilterValues",response.getReturnValue());
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
    searchClinics : function(component, event, helper) {
        try{
            var clinic_1 = component.get("v.clinic");
            var specialty_1 = component.get("v.specialty");
            var doctor_1 = component.get("v.doctor");
            var zipcode_1 = component.get("v.zipcode");
            console.log("clinic_1--> "+clinic_1);
            console.log("specialty_1--> "+specialty_1);
            console.log("doctor_1--> "+doctor_1);
            console.log("zipcode_1--> "+zipcode_1);
            var action = component.get("c.searchClinicsInformation");
            action.setParams({
                "clinicName":clinic_1,
                "specialty":specialty_1,
                "doctorName":doctor_1,
                "zipcode":zipcode_1
            });
            
            action.setCallback(this,function(response){
                var state = response.getState();
                console.log('state--> '+state);
                if(state=='SUCCESS'){
                    var result = response.getReturnValue();
                    console.log('-response.getReturnValue()--> '+JSON.stringify(result));
                    var doctorsList = [];
                    for(var i in result){
                        for(var j in result[i].doctors){
                            doctorsList.push(result[i].doctors[j]);
                        }
                    }
                    
                    component.set("v.DoctorsAndSpecialtiesList",doctorsList);
                    if(doctorsList.length>0){
                        component.set("v.NoDoctorFound",false);
                    }else{
                        component.set("v.NoDoctorFound",true);
                    }
                    console.log('doctorsList--> '+JSON.stringify(doctorsList));
                    helper.hideSpinner(component, event, helper);
                }else{
                    console.log('error');
                    helper.hideSpinner(component, event, helper);
                }
            })
            
            $A.enqueueAction(action);
        }catch(Err){
            console.log(Err);
            helper.hideSpinner(component, event, helper);
        }
    },
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})