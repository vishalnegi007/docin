({
    doInit : function(component, event, helper) {
        console.log('userId----> '+component.get("v.userId"));
    },
    resetPassword : function(component, event, helper) {
        console.log('userId----> '+component.get("v.userId"));
        var passComp = component.find('password');
        passComp[1].setCustomValidity('');
        passComp[1].reportValidity();
        var allValid = component.find('password').reduce(function (validSoFar, inputCmp) {
            inputCmp.reportValidity();
            return validSoFar && inputCmp.checkValidity();
        }, true);
        
        if (!allValid) {
            return;
        }
        var newPassword = component.get("v.newPassword");
        var newPasswordReEnter = component.get("v.newPasswordReEnter");
        if(newPassword!=newPasswordReEnter){
            passComp[1].setCustomValidity('Password do not match');
            passComp[1].reportValidity();
            return;
        }
        
        helper.showSpinner(component, event, helper);
        helper.resetPassword(component, event, helper);
    },
})