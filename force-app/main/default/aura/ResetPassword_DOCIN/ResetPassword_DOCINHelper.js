({
    resetPassword : function(component, event, helper) {
       var action = component.get("c.resetUserPassword");
        action.setParams({
            "userId":component.get("v.userId"),
            "password":component.get("v.newPassword")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state=='SUCCESS'){
                var result = response.getReturnValue();
                if(result==true){
                    component.set("v.resetPasswordModal",false);
                    setTimeout(function(){ 
                        window.open($A.get("$Label.c.DOCIN_Base_URL")+"Login", "_self");
                     }, 900);
                }
                
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        $A.enqueueAction(action);
    },
	 // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
    },
    
    // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})