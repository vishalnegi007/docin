({
    handleInit: function (component, event, helper) {
        var action = component.get("c.getSubscriptionPlans");
        action.setCallback(this, function (response) {
            var state = response.getState();
            console.log('state--> ' + state);
            if (state == 'SUCCESS') {
                var result = response.getReturnValue();
                console.log("v.result--> " + JSON.stringify(result));
                component.set("v.Subscriptions", result);
                helper.hideSpinner(component, event, helper);
            } else {
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })

        $A.enqueueAction(action);
    },
    // assignPermissionSet : function(component, event, helper) {
    //     var action = component.get("c.assignPermissionSet");
    //     action.setParams({
    //         "username":component.get("v.username")
    //     });
    //     action.setCallback(this,function(response){
    //         console.log('stateNew--> '+response.getState());
    //         let state = response.getState();
    //         if(state == 'SUCCESS'){
    //             helper.getAccountRecord(component, event, helper);
    //         }
    //     });
        
    //     $A.enqueueAction(action);
    // },
    getAccountRecord : function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        var action = component.get("c.getAccount");
        action.setParams({
            "accountId":component.get("v.accountId")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state--> '+state);
            if(state=='SUCCESS'){
                var result = response.getReturnValue();
                console.log("v.result--> "+JSON.stringify(result));
                component.set("v.AccountRecord",result);
                //helper.getPaymentMethods(component, event, helper);
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        
        $A.enqueueAction(action);
    },
    addCard: function(component, event, helper) {
        console.log("carObject---> "+JSON.stringify(component.get("v.CardObject")));
        console.log("AccountRecord---> "+JSON.stringify(component.get("v.AccountRecord")));
        var action = component.get("c.createPaymentMethod");
        action.setParams({
            "cardObjectString":JSON.stringify(component.get("v.CardObject")),
            "accountString":JSON.stringify(component.get("v.AccountRecord"))
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state--> '+state);
            
            if(state=='SUCCESS'){
                var result = response.getReturnValue();
                console.log('-response.getReturnValue()--> '+JSON.stringify(result));
                if(result.error!=null || result.error!=undefined){
                    component.set("v.errorMessage",result.error.message);
                }else{
                    component.set("v.showAddCardPopup", false);
                    helper.updateAccount(component, event, helper);
                    //helper.redirectCommunity(component, event, helper);
                }
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        
        $A.enqueueAction(action);
    },
    redirectCommunity: function (component, event, helper) {
        console.log('component.get("v.username")-->'+component.get("v.username"));
        console.log('component.get("v.password")-->'+component.get("v.password"));
        var action = component.get("c.loginCommunity");
        action.setParams({
            username: component.get("v.username"),
            password: component.get("v.password")
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            console.log('result---> '+result);
            });
        $A.enqueueAction(action);
    },
    
    updateAccount: function (component, event, helper) {
        var action = component.get("c.updateAcount");
        console.log("subscriptionPlanId::::"+component.get("v.subscriptionPlanId"));
        action.setParams({
            "subscriptionPlanId": component.get("v.subscriptionPlanId"),
            "accountRecord": component.get("v.AccountRecord")
        });
        action.setCallback(this,function(response){
            var result = response.getReturnValue();
            console.log('result---> '+result);
            helper.redirectCommunity(component, event, helper);
        });
        $A.enqueueAction(action);
    },
    
    // this function automatic call by aura:waiting event  
    showSpinner: function (component, event, helper) {
        // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true);
    },

    // this function automatic call by aura:doneWaiting event 
    hideSpinner: function (component, event, helper) {
        // make Spinner attribute to false for hide loading spinner    
        component.set("v.Spinner", false);
    },
})