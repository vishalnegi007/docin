({
    handleInit : function(component, event, helper) {
        var startYear = new Date().getFullYear();
        var years = [];
        for (var i = startYear; i <= startYear + 50; i++) {
            years.push(i);
        }
        console.log('years--> ' + years);
        component.set("v.ExpYears", years);
        var cardObject = {
            cardNumber: '', cardExpMonth: '', cardExpYear: '', cardCVC: ''
        };
        component.set("v.CardObject", cardObject);
        console.log("v.asdasd--> "+component.get("v.accountId"));
        helper.showSpinner(component, event, helper);
        helper.handleInit(component, event, helper);
        //helper.assignPermissionSet(component, event, helper);
        helper.getAccountRecord(component, event, helper);
    },
    redirectCommunity: function(component, event, helper) {
        helper.showSpinner(component, event, helper);
        helper.redirectCommunity(component, event, helper);
    },
    addCard: function (component, event, helper) {
        var allValid = component.find('cardValue').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && inputCmp.get('v.validity').valid;
        }, true);

        if (allValid) {
            helper.showSpinner(component, event, helper);
            helper.addCard(component, event, helper);
        }
    },
    /*addCard: function(component, event, helper) {
        console.log("carObject---> "+JSON.stringify(component.get("v.CardObject")));
        var action = component.get("c.createPaymentMethod");
        action.setParams({
            "cardObjectString":JSON.stringify(component.get("v.CardObject")),
            "accountRecord":component.get("v.AccountRecord")
        });
        
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('state--> '+state);
            if(state=='SUCCESS'){
                var result = response.getReturnValue();
                console.log('-response.getReturnValue()--> '+JSON.stringify(result));
                if(result.error!=null || result.error!=undefined){
                    component.set("v.errorMessage",result.error.message);
                }else{
                    component.set("v.showAddCardPopup", false);
                    helper.handleInit(component, event, helper);
                }
                helper.hideSpinner(component, event, helper);
            }else{
                console.log('error');
                helper.hideSpinner(component, event, helper);
            }
        })
        
        $A.enqueueAction(action);
    },*/
    addCardPopup: function (component, event, helper) {
        console.log('index:::'+event.target.dataset.id);
        component.set("v.subscriptionPlanId", event.target.dataset.id);
        component.set("v.showAddCardPopup", true);
    },

    closeCardPopup: function (component, event, helper) {
        component.set("v.showAddCardPopup", false);
    },
    
})