({
	handleInit : function(component, event, helper) {
		//Create Dynamic Component
        $A.createComponent('c:FindADoctorComponent', {
            
        }, function attachModal(cmp, status) {
            if (component.isValid() && status === 'SUCCESS') {
                var body = component.get("v.body");
                body.push(cmp);
                component.set("v.body", body);    
            }
        });
	},
    viewProfile : function(component, event, helper) {
        console.log("IN PARENT");
        var doctorId = event.getParam("DoctorId");
		//Create Dynamic Component
        $A.createComponent('c:DoctorProfile', {
            "DoctorId":doctorId
        }, function attachModal(cmp, status) {
            if (component.isValid() && status === 'SUCCESS') {
                var body = [];
                body.push(cmp);
                component.set("v.body", body);    
            }
        });
	},
    openAppointment: function(component, event, helper) {
        console.log("IN PARENT");
        var doctorId = event.getParam("DoctorId");
		//Create Dynamic Component
        $A.createComponent('c:BookAnAppointment', {
            "DoctorId":doctorId
        }, function attachModal(cmp, status) {
            if (component.isValid() && status === 'SUCCESS') {
                var body = [];
                body.push(cmp);
                component.set("v.body", body);    
            }
        });
	},
})