import { LightningElement ,track,api,wire} from 'lwc';
import deleteServiceRecord from '@salesforce/apex/MassEditServiceController.deleteServiceRecord';
import getServiceWithPriceInfo from '@salesforce/apex/MassEditServiceController.getServiceWithPriceInfo';
import saveService from '@salesforce/apex/MassEditServiceController.saveService';
import getPicklistValue from '@salesforce/apex/MassEditServiceController.getPicklistValue';
import getContactList from '@salesforce/apex/MassEditServiceController.getContactList';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
export default class MassEditServiceRecord extends LightningElement {

		@api recordId;
		@track keyIndex = 0;  
		@track error;
		@track message;
		@track serviceRecList;
		@api Specialty;
		@api Description;
		@api ExistingPatient;
		@api NewPatient;
		
		@api objectName = 'Service_Provider__c';
		@api fieldName = 'Specialty__c';
		@track fieldLabel;
		@api recordTypeId;
		@api value;
		@track visitOptions;
		@track selectTargetValues;
		@track displayVisitTypeColumn = false;
		@track count = 0;
		@track validate = true;
		apiFieldName;
	  
	/*---------------------------------------------------------------------------*/

	contactValues(){

		getContactList({recId: this.recordId}).then(result => {
			console.log('-----');	
			console.log(result);
			this.selectTargetValues = result;
		})
		.catch(error => {
			console.log('error', JSON.stringify(this.error));
		});

	}
	
	/*---------------------------------------------------------------------------*/

	addRow() {
		this.keyIndex+1;   
		this.serviceRecList.push ({
			VisitType:'',
			ExistingPatient:0,
			NewPatient:0
		});
	}

	/*---------------------------------------------------------------------------*/

	removeRow(event){    
				
		let serviceId  = event.target.dataset.service;
		console.log(serviceId);
		if(this.serviceRecList.length>=1){             
			this.serviceRecList.splice(event.target.accessKey,1);
			this.keyIndex-1;
		}
				
		if(serviceId != null && serviceId !=''){
			deleteServiceRecord({recId: serviceId}).then((result)=>{
				console.log(result);
			})
			.catch(error=>{
				window.console.log(error);
			})
		}

	}  
	
	/*---------------------------------------------------------------------------*/

	connectedCallback(){
		this.picklistValueInfo();
		this.contactValues();
		this.allInformation();
	}

	/*---------------------------------------------------------------------------*/
		
	allInformation(){

		getServiceWithPriceInfo({recId: this.recordId}).then(result => {
			for(let element of result){
                if(element.VisitTypeName === 'Other') {
                    this.count = parseInt(this.count + 1);
                    element.displayVisitTypeNameField = true;
                }
                else {
                    element.displayVisitTypeNameField = false;
                }
			}
            
            if(this.count > 0){
                this.displayVisitTypeColumn = true;
            }

			this.serviceRecList = result;
		})
		.catch(error => {
			console.log('error', JSON.stringify(this.error));
		});

	}

	/*---------------------------------------------------------------------------*/

	changeHandler(event){       
		if(event.target.name==='visitTypeName'){

			if(event.target.value === 'Other') {
				if(this.serviceRecList[event.target.accessKey].VisitTypeName !== 'Other') {
                	this.count = parseInt(this.count + 1);
            	}
				this.serviceRecList[event.target.accessKey].displayVisitTypeNameField = true;
			}
			else {
            	if(this.serviceRecList[event.target.accessKey].displayVisitTypeNameField === true) {
                	this.count = parseInt(this.count - 1);
            	}
            	this.serviceRecList[event.target.accessKey].displayVisitTypeNameField = false;
        	}
			
			console.log('count:::'+this.count);
			
			if(this.count > 0){
            	this.displayVisitTypeColumn = true;
        	}
        	else {
            	this.displayVisitTypeColumn = false;
        	}
				this.displayVisitTypeColumn = true;
			this.serviceRecList[event.target.accessKey].VisitTypeName = event.target.value;

		} else if(event.target.name==='visitType'){
			this.serviceRecList[event.target.accessKey].VisitType = event.target.value;
		} else if(event.target.name==='existingPatient'){
			this.serviceRecList[event.target.accessKey].ExistingPatient = event.target.value;
		} else if(event.target.name==='newPatient'){
			this.serviceRecList[event.target.accessKey].NewPatient = event.target.value;
		}
	}

	/*---------------------------------------------------------------------------*/

	saveMultipleRecords() {

        console.log('addressRecList'+JSON.stringify(this.serviceRecList));
		console.log('this.recordId--> '+this.recordId);
		
		let result = this.template.querySelectorAll('[data-name="visitType"]');
		for(let element of result){
			console.log('element:::'+element.value);
			if(!element.value) {
				element.setCustomValidity("Complete this field.");
                element.reportValidity();
				this.validate = false;
			}
			else {
				element.setCustomValidity("");
                element.reportValidity();
			}
		}
		
		console.log('this.validate--> '+this.validate);

		let resultNew = this.template.querySelectorAll('[data-name="visitTypeName"]');
		console.log('resultNew:::'+resultNew);
		for(let element of resultNew){
			console.log('element:::'+element.value);
			if(!element.value) {
				element.setCustomValidity("Choose one option!");
                element.reportValidity();
				this.validate = false;
			}
			else {
				element.setCustomValidity("");
                element.reportValidity();
			}
		}

		console.log('this.validate1--> '+this.validate);

		if(this.validate){
			for(let element of this.serviceRecList){
            	if(element.VisitTypeName !== 'Other') {
                	element.VisitType = element.VisitTypeName;
            	}
        	}

			saveService({recId: this.recordId, serviceJson : JSON.stringify(this.serviceRecList) }).then(result => {
					
				this.error = undefined;                
				this.allInformation();
				this.dispatchEvent(
					new ShowToastEvent({
						title: 'Success',
						message: 'Records save successfully',
						variant: 'success',
					}),
				);
					
				this.areDetailsVisible = false;

				this.picklistValueInfo();
				this.contactValues();
				this.allInformation();
			})
			.catch(error => {
				this.message = undefined;
				this.error = error;
				this.dispatchEvent(
					new ShowToastEvent({
						title: 'Error creating records',
						message: error.body.message,
						variant: 'error',
					}),
				);
				console.log('error', JSON.stringify(this.error));
			});
		}
		this.validate = true;

    }

	/*---------------------------------------------------------------------------*/

	picklistValueInfo(){	
		getPicklistValue({objectName: 'Service_Provider__c', fieldName: 'Visit_Type_Name__c'}).then((result)=>{
			console.log('result123::'+JSON.stringify(result));
			let options = [];
            for(let key in result) {
				let optionsValue = {};
				optionsValue["label"] = result[key];
				optionsValue["value"] = key;
				options.push(optionsValue);
			}
			this.visitOptions = options;
        })
        .catch(error=>{
            window.console.log(error);
        })
  	}

	/*---------------------------------------------------------------------------*/
}